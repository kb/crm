# -*- encoding: utf-8 -*-
from django.urls import re_path

from .views import TicketAPIView


urlpatterns = [
    re_path(r"^ticket/$", view=TicketAPIView.as_view(), name="api.crm.ticket"),
    re_path(
        r"^ticket/(?P<pk>\d+)/$",
        view=TicketAPIView.as_view(),
        name="api.crm.ticket",
    ),
]
