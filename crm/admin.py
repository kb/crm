# -*- encoding: utf-8 -*-
from django.contrib import admin

from .models import Industry, Priority


class IndustryAdmin(admin.ModelAdmin):
    pass


admin.site.register(Industry, IndustryAdmin)


class PriorityAdmin(admin.ModelAdmin):
    pass


admin.site.register(Priority, PriorityAdmin)
