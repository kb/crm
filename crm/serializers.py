# -*- encoding: utf-8 -*-
from rest_framework_json_api import serializers

from .models import Ticket, Priority


class TicketSerializer(serializers.ModelSerializer):
    contact_name = serializers.ReadOnlyField(source="contact.get_full_name")
    priority_name = serializers.ReadOnlyField(source="priority.name")
    # patrick = serializers.ReadOnlyField(source="user_assigned.username")
    user_assigned_name = serializers.SerializerMethodField()

    class Meta:
        model = Ticket
        fields = (
            "complete",
            "contact",
            "contact_name",
            "created",
            "description",
            "due",
            "id",
            "priority",
            "priority_name",
            "title",
            "user",
            "user_assigned",
            "user_assigned_name",
        )

    def get_user_assigned_name(self, instance):
        result = ""
        if instance.user_assigned:
            result = instance.user_assigned.get_full_name()
        return result


class PrioritySerializer(serializers.ModelSerializer):
    class Meta:
        model = Priority
        fields = (
            "name",
            "id",
            "level",
        )
