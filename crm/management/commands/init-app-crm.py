# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from crm.models import Funded, KanbanColumn, KanbanLane
from crm.reports import ContactTicketCountReport


class Command(BaseCommand):
    help = "Initialise crm application"

    def _init_funded(self):
        Funded.objects.init_funded(
            Funded.FREE_OF_CHARGE, "Free of Charge", True, False
        )
        Funded.objects.init_funded(
            Funded.SALES_ORDER, "Sales Order", False, True
        )

    def _init_kanban(self):
        # admin
        admin = KanbanLane.objects.init_kanban_lane("Admin", 0, False)
        KanbanColumn.objects.init_kanban_column(admin, "Backlog", 0, "")
        # development
        dev = KanbanLane.objects.init_kanban_lane("Development", 1, True)
        KanbanColumn.objects.init_kanban_column(dev, "Backlog", 0, "")
        KanbanColumn.objects.init_kanban_column(
            dev,
            "Specify",
            1,
            (
                "Is done when "
                "each task is estimated to take less than 2 days to complete"
            ),
        )
        KanbanColumn.objects.init_kanban_column(
            dev,
            "Implement",
            2,
            ("Is done when " "test coverage is higher than 60%"),
        )
        KanbanColumn.objects.init_kanban_column(
            dev,
            "Validate",
            3,
            ("Is done when " "code is installed on the client site"),
        )
        # sales
        sales = KanbanLane.objects.init_kanban_lane("Sales", 0, False)
        KanbanColumn.objects.init_kanban_column(sales, "Backlog", 0, "")
        # support
        sup = KanbanLane.objects.init_kanban_lane("Support", 2, True)
        KanbanColumn.objects.init_kanban_column(sup, "Backlog", 0, "")
        KanbanColumn.objects.init_kanban_column(sup, "Specify", 1, "")
        KanbanColumn.objects.init_kanban_column(sup, "Implement", 2, "")
        KanbanColumn.objects.init_kanban_column(sup, "Validate", 3, "")

    def _init_reports(self):
        result = ContactTicketCountReport().init_report()
        self.stdout.write(f"init_report: {result}")

    def handle(self, *args, **options):
        self.stdout.write(f"{self.help}")
        self._init_funded()
        self._init_kanban()
        self._init_reports()
        self.stdout.write(f"{self.help} - Complete")
