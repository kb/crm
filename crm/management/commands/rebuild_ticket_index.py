# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from crm.tasks import rebuild_ticket_index


class Command(BaseCommand):
    help = "Rebuild ticket index"

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        rebuild_ticket_index()
        self.stdout.write("{} - Complete".format(self.help))
