# -*- encoding: utf-8 -*-
import pandas

from django_rich.management import RichCommand
from django.utils import timezone
from dateutil.relativedelta import relativedelta

from crm.models import Ticket


class Command(RichCommand):
    help = "pandas - contact tickets #7510"

    def add_arguments(self, parser):
        parser.add_argument("months", type=int)

    def handle(self, *args, **options):
        """Count the number of tickets by month for a contact.

        Learn about pandas
        https://realpython.com/pandas-groupby/
        For management reports
        https://www.kbsoftware.co.uk/crm/ticket/7510/

        """
        months = options["months"]
        minus_x_months = timezone.now() + relativedelta(months=-months, day=1)
        self.console.print(f"[bold blue]{self.help} - since {minus_x_months}")
        data = []
        with self.console.status("Tickets...") as status:
            count = 0
            for ticket in (
                Ticket.objects.tickets()
                .filter(created__date__gte=minus_x_months)
                .order_by("contact__user__username", "pk")
            ):
                count = count + 1
                if count % 1000 == 0:
                    status.update(f"{count}...")
                data.append(
                    [
                        ticket.contact.pk,
                        ticket.contact.user.username,
                        ticket.contact.full_name,
                        ticket.pk,
                        ticket.title,
                        ticket.created,
                    ]
                )
        df = pandas.DataFrame(
            data,
            columns=[
                "contact",
                "username",
                "name",
                "ticket",
                "title",
                "created",
            ],
        )
        self.console.print(f"[bold green]df.tail()")
        df_created = df.set_index("created")
        summary_df = (
            df_created.groupby(
                [
                    "contact",
                    "username",
                    "name",
                    df_created.index.year,
                    df_created.index.month,
                ]
            )
            .agg(ticket_count_for_month=("ticket", "count"))
            .rename_axis(["contact", "username", "name", "year", "month"])
        )
        self.console.print(summary_df)
        file_name_time = timezone.now().strftime("%Y-%m-%d")
        minus_x_months_as_str = minus_x_months.strftime("%Y-%m-%d")
        file_name = f"{file_name_time}-contact-ticket-since-{minus_x_months_as_str}.xlsx"
        with pandas.ExcelWriter(file_name) as writer:
            summary_df.to_excel(writer, sheet_name="Time Records")
        self.console.print(
            f"[bold blue]{self.help} - summary since "
            f"{minus_x_months_as_str} written to '{file_name}'"
        )
