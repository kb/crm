# -*- encoding: utf-8 -*-
import urllib.parse

from django.conf import settings
from django.db.models import Count

from contact.models import Contact
from crm.forms import ReportParametersKanbanLaneForm
from crm.models import CrmContact, KanbanLane, Ticket
from report.forms import ReportParametersEmptyForm
from report.models import ReportError
from report.service import ReportMixin


class ContactTicketCountReport(ReportMixin):

    REPORT_SLUG = "crm-contact-ticket-count"
    REPORT_TITLE = "Contact List (with a count of tickets)"
    form_class = ReportParametersEmptyForm

    def run_csv_report(self, csv_writer, parameters=None):
        count = 0
        csv_writer.writerow(
            (
                "id",
                "name",
                "industry",
                "email",
                "website",
                "phone",
                "created",
                "url",
                "ticket_count",
            )
        )
        contacts = (
            Contact.objects.current()
            .annotate(count_tickets=Count("ticket_contact"))
            .order_by("user__username")
        )
        for x in contacts:
            count = count + 1
            absolute_url = x.get_absolute_url()
            industry = ""
            try:
                if x.crmcontact and x.crmcontact.industry:
                    industry = x.crmcontact.industry.name
            except CrmContact.DoesNotExist:
                pass
            url = urllib.parse.urljoin(settings.HOST_NAME, absolute_url)
            csv_writer.writerow(
                [
                    x.pk,
                    x.full_name,
                    industry,
                    x.email(),
                    x.website,
                    x.phone(),
                    x.created.strftime("%d/%m/%Y"),
                    url,
                    x.count_tickets,
                ]
            )
        return count

    def user_passes_test(self, user):
        return user.is_staff


class TicketsByKanbanLaneReport(ReportMixin):
    """Tickets by Kanban Lane.

    Management command::

      crm/management/commands/tickets-by-kanban-lane.py

    https://www.kbsoftware.co.uk/crm/ticket/7568/

    """

    REPORT_SLUG = "crm-tickets-by-kanban-lane"
    REPORT_TITLE = "Tickets by Kanban Lane"
    form_class = ReportParametersKanbanLaneForm

    def _check_parameters(self, parameters):
        if not parameters:
            raise ReportError("Cannot run report without any parameters")
        kanban_lane = parameters.get("kanban_lane")
        return kanban_lane

    def _tickets(self, title):
        try:
            kanban_lane = KanbanLane.objects.get(title=title)
        except KanbanLane.DoesNotExist:
            titles = [x.title for x in KanbanLane.objects.all()]
            raise ReportError(
                f"'KanbanLane' '{title}' does not exist. "
                f"Choose from one of the following {titles}"
            )
        return (
            Ticket.objects.current()
            .filter(kanbancard__column__lane=kanban_lane)
            .order_by("contact__user__username")
        )

    def run_csv_report(self, csv_writer, parameters=None):
        count = 0
        csv_writer.writerow(
            ("contact", "ticket", "description", "created", "url")
        )
        title = self._check_parameters(parameters)
        tickets = self._tickets(title)
        for ticket in tickets:
            count = count + 1
            absolute_url = ticket.get_absolute_url()
            url = urllib.parse.urljoin(settings.HOST_NAME, absolute_url)
            csv_writer.writerow(
                [
                    ticket.contact.full_name,
                    f"{ticket.number}",
                    ticket.title,
                    ticket.created.strftime("%d/%m/%Y"),
                    url,
                ]
            )
        return count

    def user_passes_test(self, user):
        return user.is_staff
