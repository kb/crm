# -*- encoding: utf-8 -*-
import urllib.parse

from collections import deque
from datetime import date
from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.db import models, transaction
from django.db.models import F, Q
from django.urls import reverse
from django.utils import timezone
from mptt.models import MPTTModel, TreeForeignKey, TreeManager
from reversion import revisions as reversion

from base.model_utils import TimedCreateModifyDeleteModel, TimeStampedModel
from base.singleton import SingletonModel
from chat.models import Channel, Message


class CrmError(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value

    def __str__(self):
        return repr("%s, %s" % (self.__class__.__name__, self.value))


class FundedManager(models.Manager):
    def _create_funded(self, slug, name, free_of_charge, sales_order):
        x = self.model(
            slug=slug,
            name=name,
            free_of_charge=free_of_charge,
            sales_order=sales_order,
        )
        x.save()
        return x

    def init_funded(self, slug, name, free_of_charge, sales_order):
        try:
            x = self.model.objects.get(slug=slug)
            x.name = name
            x.free_of_charge = free_of_charge
            x.sales_order = sales_order
            x.save()
        except self.model.DoesNotExist:
            x = self._create_funded(slug, name, free_of_charge, sales_order)
        return x


class Funded(TimedCreateModifyDeleteModel):
    """How is this ticket funded?

    - *FOC* is used when we do a piece of work for a client for free!
    - *Sales order* is for a support contract or for one-off work.

    Legacy:

    - *Hourly* work is for work charged at an hourly rate.
      I think this will always be covered by a sales order / contract.
    - *Internal* is used to indicate free of charge work done for KB.
      I think internal will just be *free of charge* on the KB client.

    """

    FREE_OF_CHARGE = "foc"
    # INTERNAL = "INTERNAL"
    SALES_ORDER = "sales-order"

    FUNDED = (
        (FREE_OF_CHARGE, "Free of Charge"),
        # (LEGACY, "Legacy"),
        # (HOURLY, "Hourly Rate"),
        # (INTERNAL, "Internal (FOC)"),
        (SALES_ORDER, "Sales Order"),
    )

    slug = models.SlugField(
        max_length=12, choices=FUNDED, default=FREE_OF_CHARGE
    )
    name = models.CharField(max_length=100)
    free_of_charge = models.BooleanField(default=False)
    sales_order = models.BooleanField(default=False)
    objects = FundedManager()

    class Meta:
        ordering = ("name",)
        verbose_name = "Funded"

    def __str__(self):
        return "{}".format(self.name)


class Industry(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        ordering = ("name",)
        verbose_name = "Industry"
        verbose_name_plural = "Industries"

    def __str__(self):
        return "{}".format(self.name)


reversion.register(Industry)


class CrmContact(TimeStampedModel):
    contact = models.OneToOneField(
        settings.CONTACT_MODEL, on_delete=models.CASCADE
    )
    industry = models.ForeignKey(
        Industry, blank=True, null=True, on_delete=models.CASCADE
    )

    class Meta:
        verbose_name = "CRM Contact"
        verbose_name_plural = "CRM Contacts"

    def __str__(self):
        result = "{}".format(self.contact.full_name)
        if self.industry:
            result = "{}: {}".format(result, self.industry.name)
        return result

    def get_absolute_url(self):
        return self.contact.get_absolute_url()


reversion.register(CrmContact)


class KanbanLaneManager(models.Manager):
    def create_kanban_lane(self, title, order, funded=None):
        if funded is None:
            funded = False
        obj = self.model(title=title, order=order, funded=funded)
        obj.save()
        return obj

    def init_kanban_lane(self, title, order, funded=None):
        if funded is None:
            funded = False
        try:
            obj = self.model.objects.get(title=title)
            obj.order = order
            obj.funded = funded
            obj.save()
        except self.model.DoesNotExist:
            obj = self.create_kanban_lane(title, order, funded)
        return obj

    def lanes(self):
        """Return the lanes for the Kanban board.

        .. note:: Lanes with ``order`` ``0`` are not shown on the Kanban board.

        """
        return self.model.objects.exclude(order=0).order_by("order")


class KanbanLane(models.Model):
    """Kanban lane.

    - The ``title`` will typically be for development work, support or
       background tasks.
    - The lane will be included on the Kanban board if the ``order`` is
      anything other than ``0``.  We allow lanes with an ``order`` of 0 because
      they are used for the ticket type.

    """

    title = models.CharField(max_length=100)
    order = models.IntegerField()
    funded = models.BooleanField(help_text="Should tickets be funded?")
    objects = KanbanLaneManager()

    class Meta:
        ordering = ("order", "title")
        verbose_name = "Kanban Lane"
        verbose_name_plural = "Kanban Lanes"

    def __str__(self):
        return "{}".format(self.title)

    def backlog(self):
        """Backlog tickets for this Kanban lane.

        - Kanban cards with an ``order`` of ``0`` are pending i.e. not on the
          backlog yet.

        """
        backlog = self.backlog_column()
        cards = KanbanCard.objects.filter(column=backlog)
        return (
            Ticket.objects.current()
            .filter(kanbancard__in=cards)
            .exclude(kanbancard__order=0)
            .order_by("kanbancard__order", "kanbancard__created")
        )

    def backlog_column(self):
        try:
            return self.kanbancolumn_set.get(order=0)
        except KanbanColumn.DoesNotExist:
            raise CrmError(
                "Kanban lane '{}' has no backlog column".format(self.title)
            )

    def backlog_pending(self):
        """Tickets which can be added to the backlog for this Kanban lane.

        - Kanban cards with an ``order`` of ``0`` are pending i.e. not on the
          backlog yet.

        """
        backlog = self.backlog_column()
        cards = KanbanCard.objects.filter(column=backlog)
        return (
            Ticket.objects.current()
            .filter(kanbancard__in=cards, kanbancard__order=0)
            .order_by("kanbancard__created")
        )

    def columns(self):
        """Return the columns for this swimlane.

        .. note:: Column with ``order`` ``0`` is the backlog, so is excluded.

        """
        return self.kanbancolumn_set.exclude(order=0).order_by("order")

    def first_column(self):
        """Return the first Kanban column for this swimlane.

        .. note:: Column with ``order`` ``0`` is the backlog, so is excluded.

        """
        result = self.columns().first()
        if not result:
            raise CrmError(
                "Kanban lane '{}' has no visible columns".format(self.title)
            )
        return result

    def last_column(self):
        """Return the last Kanban column for this swimlane."""
        result = self.columns().last()
        if not result:
            raise CrmError(
                "Kanban lane '{}' has no visible columns".format(self.title)
            )
        return result


class KanbanColumnManager(models.Manager):
    def create_kanban_column(self, lane, title, order, help_text):
        obj = self.model(
            lane=lane, title=title, order=order, help_text=help_text
        )
        obj.save()
        return obj

    def init_kanban_column(self, lane, title, order, help_text):
        try:
            obj = self.model.objects.get(lane=lane, title=title)
            obj.order = order
            obj.help_text = help_text
            obj.save()
        except self.model.DoesNotExist:
            obj = self.create_kanban_column(lane, title, order, help_text)
        return obj


class KanbanColumn(models.Model):
    """Kanban column, typically, 'Specify', 'Implement' and 'Validate'.

    ``order`` ``0`` is used for the ``backlog`` i.e. not shown on Kanban board.

    """

    lane = models.ForeignKey(KanbanLane, on_delete=models.CASCADE)
    title = models.CharField(max_length=100)
    # This is trying to make it easy to create the grid on the template??
    order = models.IntegerField()
    help_text = models.TextField(blank=True)
    objects = KanbanColumnManager()

    class Meta:
        ordering = ("lane", "order")
        unique_together = ("lane", "order")
        verbose_name = "Kanban Column"
        verbose_name_plural = "Kanban Columns"

    def __str__(self):
        return "{} - {}".format(self.title, self.lane.title)

    def _tickets(self, state):
        return (
            Ticket.objects.current()
            .filter(kanbancard__column=self)
            .filter(kanbancard__state=state)
        )

    def is_backlog(self):
        return self.order == 0

    def is_final(self):
        """Is this the last column in the lane?"""
        columns = self.lane.columns()
        return self == columns.last()

    def is_first(self):
        """Is this the first column in the lane?"""
        return self == self.lane.first_column()

    def tickets_active(self):
        return self._tickets(KanbanCard.ACTIVE)

    def tickets_done(self):
        return self._tickets(KanbanCard.DONE)

    def tickets_tracked(self):
        return self._tickets(KanbanCard.TRACKED)


class Priority(models.Model):
    """Level 0 will exclude the ticket from normal reminder lists"""

    name = models.CharField(max_length=100, unique=True)
    level = models.IntegerField()

    class Meta:
        ordering = ("-level", "name")
        verbose_name = "Priority"
        verbose_name_plural = "Priorities"

    def __str__(self):
        return "{}".format(self.name)


reversion.register(Priority)


class TicketManager(TreeManager):
    def completed(self, contact=None):
        return self.tickets(contact).exclude(complete__isnull=True)

    def current(self, contact=None):
        return self.tickets(contact).filter(complete__isnull=True)

    def excluding_project_children(self, contact):
        """A list of current tickets and top level MPTT parent tickets.

        We exclude any project (development) tickets with a parent.

        https://www.kbsoftware.co.uk/crm/ticket/3738/

        """
        qs = Ticket.objects.tickets(contact)
        crm_settings = CrmSettings.load()
        if crm_settings.project_lane:
            # development (parent tickets only)
            q_dev_parent = Q()
            q_dev_parent &= Q(parent__isnull=True)
            q_dev_parent &= Q(
                kanbancard__column__lane=crm_settings.project_lane
            )
            # other tickets
            q_other = Q()
            q_other &= ~Q(kanbancard__column__lane=crm_settings.project_lane)
            # this is the query!
            qs = qs.filter(q_other | q_dev_parent)
        return qs

    def favourites(self, user):
        qs = UserTicketFavourite.objects.favourites(user).select_related(
            "ticket"
        )
        pks = [x.ticket.pk for x in qs]
        return self.current().filter(pk__in=pks)

    def notify(self):
        """List of tickets, with a due date, which need to be notified."""
        return self.current().filter(
            Q(due_notify__lte=date.today())
            | (Q(due__isnull=False) & Q(due_notify__isnull=True))
        )

    def planner(self):
        return (
            self.tickets()
            .filter(complete__isnull=True)
            .exclude(priority__level=0)
        )

    def project_children(self, contact=None):
        """A list of project (development) tickets with a parent.

        .. tip:: This is the tickets which are not included by the
                 ``excluding_project_children`` method.

        """
        crm_settings = CrmSettings.load()
        if crm_settings.project_lane:
            qs = (
                self.tickets(contact)
                .filter(parent__isnull=False)
                .filter(kanbancard__column__lane=crm_settings.project_lane)
            )
        else:
            raise CrmError(
                "Cannot find 'project_children' unless CRM "
                "settings are created."
                ""
            )
        return qs

    def project_no_parent(self, contact=None):
        """Project tickets without a parent.

        https://stackoverflow.com/questions/46596460/django-mptt-filter-only-if-no-children-exist

        """
        crm_settings = CrmSettings.load()
        if crm_settings.project_lane:
            qs = (
                self.tickets(contact)
                .filter(parent__isnull=True)
                .filter(kanbancard__column__lane=crm_settings.project_lane)
                .filter(lft=F("rght") - 1)
            )
        else:
            raise CrmError(
                "Cannot find 'project_children' unless CRM "
                "settings are created."
            )
        return qs

    def tickets(self, contact=None):
        """Exclude deleted tickets."""
        qs = self.model.objects.exclude(deleted=True)
        if contact is None:
            pass
        else:
            qs = qs.filter(contact=contact)
        return qs


class Ticket(MPTTModel):
    """Ticket.

    Kanban

    - The ``Ticket``
      is linked to a ``KanbanCard`` which
      is linked to a ``KanbanColumn`` which
      is linked to a ``KanbanLane`` which
      is used as the ``ticket_type`` (see the ``TicketForm``)
      e.g. *Development*, *Sales*, *Support* or *Admin* (``init_app_crm``).
    - If the ``KanbanColumn`` is a backlog column (``order`` is ``0``) and the
      ``kanban_order`` is ``0`` then the ticket has not been added to the
      backlog.

    Funded

    - If the ``funded`` column in the ``KanbanLane`` is set to ``True``, then
      the ``funded`` column in the ``Ticket`` model will need to be set before
      ``SalesOrderTicket`` reports can be generated.
    - The ``funded`` field will be replacing the ``fixed_price`` field.

    .. note:: For more information, see ``exclude_fixed_price_free_of_charge``
              in the ``TimeRecordManager``.

    """

    STATUS_ALL = 1
    STATUS_CURRENT = 2
    STATUS_COMPLETE = 3

    TYPE_ALL = -1
    TYPE_DEFAULT = -2
    TYPE_PROJECT_CHILDREN = -3
    TYPE_PROJECT_NO_PARENT = -4

    contact = models.ForeignKey(
        settings.CONTACT_MODEL,
        related_name="ticket_contact",
        on_delete=models.CASCADE,
    )
    parent = TreeForeignKey(
        "self",
        null=True,
        blank=True,
        related_name="children",
        db_index=True,
        on_delete=models.CASCADE,
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name="+", on_delete=models.CASCADE
    )
    title = models.CharField(max_length=100)
    description = models.TextField(blank=True, null=True)
    priority = models.ForeignKey(Priority, on_delete=models.CASCADE)
    due = models.DateField(blank=True, null=True)
    due_notify = models.DateField(
        blank=True, null=True, help_text="Notify Due Date"
    )
    complete = models.DateTimeField(blank=True, null=True)
    complete_user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        blank=True,
        null=True,
        related_name="+",
        on_delete=models.CASCADE,
    )
    user_assigned = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        blank=True,
        null=True,
        related_name="+",
        on_delete=models.CASCADE,
    )
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    deleted = models.BooleanField(default=False)
    date_deleted = models.DateTimeField(blank=True, null=True)
    user_deleted = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        blank=True,
        null=True,
        related_name="+",
        on_delete=models.CASCADE,
    )
    fixed_price = models.BooleanField(default=False)
    funded = models.ForeignKey(
        Funded, blank=True, null=True, on_delete=models.CASCADE
    )
    objects = TicketManager()

    class Meta:
        ordering = ("-complete", "due", "-priority__level", "created")
        verbose_name = "Ticket"
        verbose_name_plural = "Tickets"

    class MPTTMeta:
        order_insertion_by = ["created"]

    def __str__(self):
        return "{}".format(self.title)

    def can_complete(self):
        card = self.kanban_card()
        if card:
            result = card.can_complete()
        else:
            result = True
        return result

    def can_edit_kanban_column(self):
        """Column can be edited if the ticket is not on the Kanban board."""
        card = self.kanban_card()
        if card:
            result = card.can_edit_kanban_column()
        else:
            result = True
        return result

    def chat(self, caption, user):
        absolute_url = self.get_absolute_url()
        ticket_url = urllib.parse.urljoin(settings.HOST_NAME, absolute_url)
        # restrict the length of the contact name
        contact_name = self.contact.full_name
        if len(contact_name) > 30:
            contact_name = "{} ...".format(contact_name[:26])
        else:
            contact_name = "{},".format(contact_name)
        # restrict the length of the ticket title
        ticket_title = self.title
        if len(ticket_title) > 40:
            ticket_title = "{} ...".format(ticket_title[:36])
        else:
            ticket_title = "{},".format(ticket_title)
        # queue the message
        self.create_chat_message(
            user,
            "[Ticket #{}]({}), {} {} {} (by *{}*)".format(
                self.pk,
                ticket_url,
                contact_name,
                ticket_title,
                caption,
                user.username,
            ),
        )

    def create_chat_message(self, user, title):
        channel = Channel.objects.get(slug=Channel.ACTIVITY)
        Message.objects.create_message(channel, user, title)

    def get_absolute_url(self):
        return reverse("ticket.detail", args=[self.pk])

    def get_summary_description(self):
        return filter(None, (self.title, self.description))

    def kanban_card(self):
        result = None
        try:
            result = self.kanbancard
        except KanbanCard.DoesNotExist:
            pass
        return result

    @property
    def is_deleted(self):
        """We don't delete tickets - we only complete them.

        .. note:: This method is only used by the ``search`` app.
                  ``search.service.SearchIndex`` needs an ``is_deleted`` method.

        """
        return False

    def is_parent(self):
        """Is this ticket a parent?

        I don't think this method runs any SQL queries:

        From the documentation for ``get_descendant_count``:

          Returns the number of descendants the model instance has, based on
          its left and right tree node edge indicators. As such, this does not
          incur any database access.

        From the source code for ``is_root_node``:

          return getattr(self, self._mptt_meta.parent_attr + '_id') is None

        """
        return bool(self.is_root_node and self.get_descendant_count())

    @property
    def level_range(self):
        return range(0, self.level)

    def needs_sales_order(self):
        result = False
        if self.funded:
            if self.funded.free_of_charge:
                pass
            else:
                card = self.kanban_card()
                if card:
                    if card.column.lane.funded:
                        result = True
        return result

    @property
    def notes(self):
        return self.note_set.order_by("-created")

    @property
    def number(self):
        return "{:06d}".format(self.pk)

    def parent_contact(self):
        result = None
        root = self.get_root()
        if root and root.is_parent():
            result = root.contact
        return result

    def set_complete(self, user):
        self.complete = timezone.now()
        self.complete_user = user

    def set_uncomplete(self):
        self.complete = None
        self.complete_user = None

    @property
    def time_records(self):
        return self.timerecord_set.order_by("-created")

    @property
    def is_overdue(self):
        return self.due < date.today()


reversion.register(Ticket)


class KanbanCardManager(models.Manager):
    def create_kanban_card(self, ticket, column, user, state=None):
        """Create a Kanban record.

        .. note:: This method expects a ``KanbanColumn``.  The init method
                  expects a ``KanbanLane``.

        - The ``user`` is for the audit.

        """
        card = self.model(ticket=ticket, column=column)
        if state:
            card.state = state
        card.save()
        KanbanAudit.objects.create_kanban_audit(user, card)
        return card

    def init_kanban_card(self, ticket, lane, user):
        """Create a Kanban record.

        .. note:: This method expects a ``KanbanLane``.  The create method
                  expects a ``KanbanColumn``.

        - The ``user`` is only for the audit - it does not assign the ticket to
          the user.

        """
        try:
            card = self.model.objects.get(ticket=ticket)
            if card.column.lane == lane:
                pass
            else:
                if card.can_edit_kanban_column():
                    card.column = lane.backlog_column()
                    card.order = 0
                    card.state = KanbanCard.ACTIVE
                    card.save()
                    KanbanAudit.objects.create_kanban_audit(user, card)
        except self.model.DoesNotExist:
            backlog_column = lane.backlog_column()
            card = self.create_kanban_card(ticket, backlog_column, user)
        return card


class KanbanCard(TimeStampedModel):
    """Kanban card.

    - The ``order`` is used for the backlog.
    - The user is the person assigned to the ticket.

    TODO

    - The ticket type is the same as the Kanban lane e.g. Support or
      Development.
    - The ticket user is the same as the Kanban user.
    - We need to audit changes to the lane and the user.

    """

    ACTIVE = "A"
    DONE = "D"
    TRACKED = "T"

    STATE = ((ACTIVE, "Active"), (DONE, "Done"), (TRACKED, "Tracked"))

    ticket = models.OneToOneField(Ticket, on_delete=models.CASCADE)
    column = models.ForeignKey(KanbanColumn, on_delete=models.CASCADE)
    state = models.CharField(max_length=1, choices=STATE, default=ACTIVE)
    order = models.IntegerField(default=0, help_text="Backlog Order")
    objects = KanbanCardManager()

    class Meta:
        ordering = ("column", "state", "order")
        verbose_name = "Kanban"
        verbose_name_plural = "Kanban"

    def __str__(self):
        return "{}".format(self.ticket.title)

    def add_to_backlog(self, user):
        if self.column.is_backlog():
            if self.order == 0:
                qs = (
                    KanbanCard.objects.filter(column=self.column)
                    .exclude(order=0)
                    .order_by("order", "created")
                )
                pks = deque(qs.values_list("pk", flat=True))
                pks.appendleft(self.pk)
                with transaction.atomic():
                    order = 1
                    for pk in pks:
                        card = KanbanCard.objects.get(pk=pk)
                        card.order = order
                        card.save()
                        KanbanAudit.objects.create_kanban_audit(user, card)
                        order = order + 1
            else:
                raise CrmError(
                    "Cannot add ticket {} to the backlog. It is not in "
                    "the pending state".format(self.ticket.pk)
                )
        else:
            raise CrmError(
                "Cannot add ticket {} to the backlog. "
                "The '{}' column is not a backlog "
                "column".format(self.ticket.pk, self.column.title)
            )

    def kanban_active(self, user):
        if self.column.is_backlog():
            raise CrmError(
                "Ticket {} is not on the Kanban board so can't be "
                "made active".format(self.ticket.pk)
            )
        else:
            with transaction.atomic():
                self.state = self.ACTIVE
                self.save()
                KanbanAudit.objects.create_kanban_audit(user, self)

    def kanban_done(self, user):
        if self.column.is_backlog():
            raise CrmError(
                "Ticket {} is not on the Kanban board so can't be "
                "marked as done".format(self.ticket.pk)
            )
        else:
            with transaction.atomic():
                self.state = self.DONE
                self.save()
                KanbanAudit.objects.create_kanban_audit(user, self)

    def kanban_first_column(self):
        """Is this card in the first column on the Kanban board?"""
        first_column = self.column.lane.columns().first()
        return bool(first_column and self.column == first_column)

    def kanban_next(self, user):
        if self.column.is_backlog():
            raise CrmError(
                "Ticket {} is not on the Kanban board so can't be "
                "moved to the next column".format(self.ticket.pk)
            )
        if self.column == self.column.lane.last_column():
            raise CrmError(
                "Ticket {} is in the last column so cannot be "
                "moved to the next column".format(self.ticket.pk)
            )
        if not self.state == self.DONE:
            raise CrmError(
                "Ticket {} is not 'done' so cannot be moved "
                "to the next column".format(self.ticket.pk)
            )
        column = found = None
        for column in self.column.lane.columns():
            if found:
                break
            if column == self.column:
                found = True
        if not column:
            raise CrmError(
                "Cannot find the next column for ticket "
                "{}".format(self.ticket.pk)
            )
        with transaction.atomic():
            self.state = self.ACTIVE
            self.column = column
            self.save()
            KanbanAudit.objects.create_kanban_audit(user, self)

    def kanban_previous(self, user):
        if self.column.is_backlog():
            raise CrmError(
                "Ticket {} is on the backlog, so cannot be "
                "moved to the previous column".format(self.ticket.pk)
            )
        if not self.state == self.ACTIVE:
            raise CrmError(
                "Ticket {} is not 'active' so cannot be moved "
                "to the previous column".format(self.ticket.pk)
            )
        column = found = None
        qs = self.column.lane.kanbancolumn_set.order_by("-order")
        for column in qs:
            if found:
                break
            if column == self.column:
                found = True
        if not column:
            raise CrmError(
                "Cannot find the previous column for ticket "
                "{}".format(self.ticket.pk)
            )
        with transaction.atomic():
            self.column = column
            self.order = 1
            self.state = self.ACTIVE
            self.save()
            KanbanAudit.objects.create_kanban_audit(user, self)

    def kanban_tracked(self, user):
        if self.column.is_backlog():
            raise CrmError(
                "Ticket {} is not on the Kanban board so can't be "
                "tracked".format(self.ticket.pk)
            )
        else:
            with transaction.atomic():
                self.state = self.TRACKED
                self.save()
                KanbanAudit.objects.create_kanban_audit(user, self)

    def move_to_kanban_board(self, user):
        if self.column.is_backlog():
            if self.order == 0:
                raise CrmError(
                    "Cannot add ticket {} to the Kanban board. It is "
                    "not on the backlog".format(self.ticket.pk)
                )
            else:
                column = self.column.lane.first_column()
                with transaction.atomic():
                    self.column = column
                    self.save()
                    KanbanAudit.objects.create_kanban_audit(user, self)
        else:
            raise CrmError(
                "Cannot move ticket {} to the Kanban board. "
                "The '{}' column is not a backlog "
                "column".format(self.ticket.pk, self.column.title)
            )

    def backlog_move_down(self, user):
        order = self.order
        qs = (
            KanbanCard.objects.filter(column=self.column)
            .filter(order__gte=order)
            .exclude(pk=self.pk)
            .exclude(order=0)
            .order_by("order", "created")
        )
        pks = deque(qs.values_list("pk", flat=True))
        first = None
        if pks:
            first = pks.popleft()
        pks.appendleft(self.pk)
        if first:
            pks.appendleft(first)
        with transaction.atomic():
            for pk in pks:
                card = KanbanCard.objects.get(pk=pk)
                card.order = order
                card.save()
                KanbanAudit.objects.create_kanban_audit(user, card)
                order = order + 1

    def backlog_move_up(self, user):
        order = self.order - 1
        if order <= 0:
            raise CrmError(
                "Ticket {} cannot be moved any higher in the "
                "backlog".format(self.ticket.pk)
            )
        qs = (
            KanbanCard.objects.filter(column=self.column)
            .filter(order__gte=order)
            .exclude(pk=self.pk)
            .exclude(order=0)
            .order_by("order", "created")
        )
        pks = deque(qs.values_list("pk", flat=True))
        pks.appendleft(self.pk)
        with transaction.atomic():
            for pk in pks:
                card = KanbanCard.objects.get(pk=pk)
                card.order = order
                card.save()
                KanbanAudit.objects.create_kanban_audit(user, card)
                order = order + 1

    def can_complete(self):
        """Can the card be completed?

        The card can be completed:

        1. If it is in the pending column of the backlog.
        2. If it is in the final column of the board and is ``DONE``.

        """
        result = False
        if self.column.is_backlog():
            result = self.order == 0
        elif self.column.is_final():
            result = self.state == self.DONE
        return result

    def can_edit_kanban_column(self):
        """Column can be edited if the ticket is not on the Kanban board."""
        result = False
        # display column on kanban board
        if self.column.order:
            # is the kanban lane displayed on the kanban board?
            if self.column.lane.order:
                pass
            else:
                result = True
        else:
            result = True
        return result

    def is_active(self):
        return self.state == self.ACTIVE

    def is_backlog(self):
        """Card is on the backlog if it is in the backlog column and it has an order."""
        result = False
        if self.column.is_backlog():
            if self.order == 0:
                pass
            else:
                result = True
        return result

    def is_done(self):
        return self.state == self.DONE

    def is_tracked(self):
        return self.state == self.TRACKED

    def remove_from_backlog(self, user):
        if self.is_backlog():
            self.order = 0
            with transaction.atomic():
                self.save()
                KanbanAudit.objects.create_kanban_audit(user, self)
        else:
            raise CrmError(
                "Cannot remove ticket '{}' from the backlog. "
                "It is not on the backlog".format(self.ticket.pk)
            )


class KanbanAuditManager(models.Manager):
    def create_kanban_audit(self, user, card):  # , column, state, order):
        obj = self.model(
            user=user,
            card=card,
            column=card.column,
            state=card.state,
            order=card.order,
        )
        obj.save()
        return obj


class KanbanAudit(models.Model):
    """Audit when a Kanban card moves from one column to another."""

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    card = models.ForeignKey(KanbanCard, on_delete=models.CASCADE)
    column = models.ForeignKey(KanbanColumn, on_delete=models.CASCADE)
    state = models.CharField(max_length=1, choices=KanbanCard.STATE)
    order = models.IntegerField(default=0)
    objects = KanbanAuditManager()

    class Meta:
        ordering = ("created", "user__username")
        verbose_name = "Kanban Audit"
        verbose_name_plural = "Kanban Audit"

    def __str__(self):
        return "{} {}".format(self.user.username, self.created)


class Note(TimeStampedModel):
    ticket = models.ForeignKey(Ticket, on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    title = models.CharField(max_length=100)
    description = models.TextField(blank=True, null=True)

    class Meta:
        ordering = ("created",)
        verbose_name = "Note"
        verbose_name_plural = "Notes"

    def __str__(self):
        return "{}".format(self.title)

    @property
    def deleted(self):
        """No actual delete (yet), so just return 'False'."""
        return False

    def get_absolute_url(self):
        return reverse("ticket.detail", args=[self.ticket.pk])

    def get_update_url(self):
        return reverse("crm.note.update", args=[self.pk])

    def get_summary_description(self):
        return filter(None, (self.title, self.description))

    @property
    def modified_today(self):
        return self.created > timezone.now() - relativedelta(hours=24)


reversion.register(Note)


class UserSettingsManager(models.Manager):
    def create_user_settings(self, user):
        obj = self.model(user=user)
        obj.save()
        return obj

    def init_user_settings(self, user):
        try:
            obj = self.model.objects.get(user=user)
        except self.model.DoesNotExist:
            obj = self.create_user_settings(user)
        return obj

    def settings(self, user):
        try:
            result = self.model.objects.get(user=user)
        except self.model.DoesNotExist:
            result = self.model()
        return result


class UserSettings(TimedCreateModifyDeleteModel):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL, unique=True, on_delete=models.CASCADE
    )
    display_notify_list = models.BooleanField(default=False)
    objects = UserSettingsManager()

    class Meta:
        ordering = ["user__username"]
        verbose_name = "User settings"
        verbose_name_plural = "User settings"

    def __str__(self):
        return "{}".format(self.user.username)


reversion.register(UserSettings)


class UserTicketFavouriteManager(models.Manager):
    def create_user_ticket_favourite(self, user, ticket):
        obj = self.model(user=user, ticket=ticket)
        obj.save()
        return obj

    def init_user_ticket_favourite(self, user, ticket):
        try:
            obj = self.model.objects.get(user=user, ticket=ticket)
            obj.save()
        except self.model.DoesNotExist:
            obj = self.create_user_ticket_favourite(user, ticket)
        return obj

    def favourite(self, user, ticket):
        with transaction.atomic():
            obj = self.init_user_ticket_favourite(user, ticket)
            if obj.is_deleted:
                obj.undelete()

    def favourites(self, user):
        return self.model.objects.filter(user=user).exclude(deleted=True)

    def is_favourite(self, user, ticket):
        result = False
        try:
            obj = self.model.objects.get(user=user, ticket=ticket)
            result = not obj.is_deleted
        except self.model.DoesNotExist:
            pass
        return result

    def unfavourite(self, user, ticket):
        with transaction.atomic():
            obj = self.init_user_ticket_favourite(user, ticket)
            obj.set_deleted(user)


class UserTicketFavourite(TimedCreateModifyDeleteModel):
    """Favourite tickets for a user."""

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    ticket = models.ForeignKey(Ticket, on_delete=models.CASCADE)
    objects = UserTicketFavouriteManager()

    class Meta:
        ordering = ["user__username", "ticket__pk"]
        unique_together = ["user", "ticket"]
        verbose_name = "User ticket"
        verbose_name_plural = "User tickets"

    def __str__(self):
        return "{} {} {}".format(
            self.user.username, self.ticket.pk, self.ticket.title
        )


reversion.register(UserTicketFavourite)


class CrmSettings(SingletonModel):
    project_lane = models.ForeignKey(
        KanbanLane,
        blank=True,
        null=True,
        help_text="Exclude these tickets",
        on_delete=models.CASCADE,
    )

    class Meta:
        verbose_name = "CRM Settings"
        verbose_name_plural = "CRM Settings"

    def __str__(self):
        return "CRM Settings: Kanban lane for project work: {}".format(
            self.project_lane.title
        )


reversion.register(CrmSettings)
