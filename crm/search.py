# -*- encoding: utf-8 -*-
from crm.models import Ticket
from search.search import Fmt


class TicketIndexMixin:
    def settings(self):
        """Index settings."""
        return {"number_of_shards": 1}

    def mappings(self):
        """Index configuration.  Used by the Elasticsearch 'create' method."""
        return {
            "properties": {
                "full_name": {"type": "text", "analyzer": "english"},
                "number": {"type": "long"},
                "title": {"type": "text", "analyzer": "english"},
                "description": {"type": "text", "analyzer": "english"},
                "notes": {"type": "text", "analyzer": "english"},
                "username": {"type": "text", "analyzer": "standard"},
            }
        }

    def data_as_fmt(self, source, pk):
        """A row of data returned as part of the search results.

        .. note:: The ``pk`` is so we can add it into the results table if we
                  want to.

        """
        return [
            [Fmt("{0:06d}".format(pk), url=True)],
            [
                Fmt("{full_name}".format(**source)),
                Fmt("{title}".format(**source), url=True),
                Fmt("{description}".format(**source), small=True),
            ],
        ]

    @property
    def index_name(self):
        """Name of the Elasticsearch index (will have the domain added)."""
        return "ticket"

    def query(self, criteria):
        """The Elasticsearch query.

        .. note:: If the ``criteria`` is a number, then we just search in
                  the ticket number field.

        """
        try:
            number = int(criteria)
            should = [{"match": {"number": number}}]
        except ValueError:
            should = [
                {"match": {"title": {"query": criteria, "boost": 2}}},
                {"match": {"description": {"query": criteria, "boost": 1}}},
                {"match": {"full_name": criteria}},
                {"match": {"notes": criteria}},
                {"match": {"username": criteria}},
            ]

        return {"query": {"bool": {"should": should}}}

    def queryset(self, pk):
        """Build the Elasticsearch index using this queryset."""
        if pk is None:
            qs = Ticket.objects.all().order_by("-pk")
        else:
            qs = Ticket.objects.filter(pk=pk)
        return qs

    def source(self, row):
        """Build the index using the data from the 'queryset'."""
        notes = []
        for x in row.notes:
            notes.append(x.title)
            if x.description:
                notes.append(x.description)
        for x in row.time_records:
            notes.append(x.title)
            if x.description:
                notes.append(x.description)
        return {
            "description": row.description,
            "full_name": row.contact.full_name,
            "notes": notes,
            "number": row.pk,
            "title": row.title,
            "username": row.contact.user.username,
        }

    def table_headings(self):
        """Table headings for the results."""
        return ["Number", "Description"]

    def url_name(self):
        """URL name for the search results."""
        return "ticket.detail"


class TicketIndex(TicketIndexMixin):
    @property
    def is_soft_delete(self):
        return False
