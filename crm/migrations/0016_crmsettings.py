# Generated by Django 2.1.7 on 2019-03-18 22:43

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [("crm", "0015_auto_20190203_0802")]

    operations = [
        migrations.CreateModel(
            name="CrmSettings",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "project_lane",
                    models.ForeignKey(
                        blank=True,
                        help_text="Exclude these tickets",
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        to="crm.KanbanLane",
                    ),
                ),
            ],
            options={
                "verbose_name": "CRM Settings",
                "verbose_name_plural": "CRM Settings",
            },
        )
    ]
