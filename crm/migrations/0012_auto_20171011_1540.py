# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-10-11 14:40
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import mptt.fields


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("crm", "0011_ticket_fixed_price"),
    ]

    operations = [
        migrations.CreateModel(
            name="UserSettings",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("created", models.DateTimeField(auto_now_add=True)),
                ("modified", models.DateTimeField(auto_now=True)),
                ("deleted", models.BooleanField(default=False)),
                ("date_deleted", models.DateTimeField(blank=True, null=True)),
                ("display_notify_list", models.BooleanField(default=False)),
                (
                    "user",
                    models.OneToOneField(
                        on_delete=django.db.models.deletion.CASCADE,
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
                (
                    "user_deleted",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="+",
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
            ],
            options={
                "ordering": ["user__username"],
                "verbose_name": "User settings",
                "verbose_name_plural": "User settings",
            },
        ),
        migrations.CreateModel(
            name="UserTicketFavourite",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("created", models.DateTimeField(auto_now_add=True)),
                ("modified", models.DateTimeField(auto_now=True)),
                ("deleted", models.BooleanField(default=False)),
                ("date_deleted", models.DateTimeField(blank=True, null=True)),
            ],
            options={
                "ordering": ["user__username", "ticket__pk"],
                "verbose_name": "User ticket",
                "verbose_name_plural": "User tickets",
            },
        ),
        migrations.AddField(
            model_name="ticket",
            name="due_notify",
            field=models.DateField(
                blank=True, help_text="Notify Due Date", null=True
            ),
        ),
        migrations.AddField(
            model_name="ticket",
            name="level",
            field=models.PositiveIntegerField(
                db_index=True, default=0, editable=False
            ),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name="ticket",
            name="lft",
            field=models.PositiveIntegerField(
                db_index=True, default=0, editable=False
            ),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name="ticket",
            name="parent",
            field=mptt.fields.TreeForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="children",
                to="crm.Ticket",
            ),
        ),
        migrations.AddField(
            model_name="ticket",
            name="rght",
            field=models.PositiveIntegerField(
                db_index=True, default=0, editable=False
            ),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name="ticket",
            name="tree_id",
            field=models.PositiveIntegerField(
                db_index=True, default=0, editable=False
            ),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name="userticketfavourite",
            name="ticket",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE, to="crm.Ticket"
            ),
        ),
        migrations.AddField(
            model_name="userticketfavourite",
            name="user",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                to=settings.AUTH_USER_MODEL,
            ),
        ),
        migrations.AddField(
            model_name="userticketfavourite",
            name="user_deleted",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="+",
                to=settings.AUTH_USER_MODEL,
            ),
        ),
        migrations.AlterUniqueTogether(
            name="userticketfavourite",
            unique_together=set([("user", "ticket")]),
        ),
    ]
