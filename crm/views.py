# -*- encoding: utf-8 -*-
from datetime import date

from braces.views import (
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    SuperuserRequiredMixin,
)
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.views.generic import (
    CreateView,
    DetailView,
    ListView,
    RedirectView,
    UpdateView,
)
from rest_framework import authentication, permissions, viewsets
from rest_framework.response import Response
from rest_framework.views import APIView

from base.url_utils import url_with_querystring
from base.view_utils import BaseMixin, RedirectNextMixin
from chat.models import Channel, Message
from chat.tasks import send_chat_messages
from contact.views import ContactPermMixin
from crm.service import get_contact_model
from crm.tasks import update_ticket_index
from invoice.models import QuickTimeRecord, ticket_activity, TimeRecord
from .forms import (
    CrmContactForm,
    CrmSettingsForm,
    IndustryForm,
    NoteForm,
    SearchForm,
    TicketCompleteForm,
    TicketEmptyForm,
    TicketFundingForm,
    TicketForm,
    TicketListSearchForm,
    TicketMoveForm,
    UserSettingsForm,
)
from .models import (
    CrmContact,
    CrmError,
    CrmSettings,
    Industry,
    KanbanCard,
    KanbanLane,
    Note,
    Ticket,
    UserSettings,
    UserTicketFavourite,
)
from .serializers import TicketSerializer


class ContactTicketListMixin:
    """List of tickets with the contact added to the context."""

    def _contact(self):
        pk = self.kwargs.get("pk")
        contact_model = get_contact_model()
        return contact_model.objects.get(pk=pk)

    def get_context_data(self, **kwargs):
        """Set the ``contact`` (pretend to be a ``DetailView``)."""
        context = super().get_context_data(**kwargs)
        # contact
        contact = self._contact()
        # crm
        try:
            crm_contact = contact.crmcontact
        except ObjectDoesNotExist:
            crm_contact = None
        # invoice
        try:
            invoice_contact = contact.invoicecontact
        except ObjectDoesNotExist:
            invoice_contact = None
        context.update(
            dict(
                contact=contact,
                crm_contact=crm_contact,
                invoice_contact=invoice_contact,
            )
        )
        context.update(contact.get_multi_as_dict())
        return context

    def get_queryset(self):
        contact = self._contact()
        return (
            Ticket.objects.excluding_project_children(contact)
            .filter(complete__isnull=True)
            .order_by("-created")
        )

    def test_contact(self):
        return self._get_contact()


class CrmContactCreateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, CreateView
):
    model = CrmContact
    form_class = CrmContactForm

    def _contact(self):
        pk = self.kwargs.get("pk")
        contact = get_contact_model().objects.get(pk=pk)
        return contact

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.contact = self._contact()
        return super().form_valid(form)


class CrmContactTicketListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    """Update this view so it is a general purpose ticket list view.

    1. Drop down ticket type (``KanbanLane``)
    1.1. Add in *default* support tickets with project parent tickets
    1.2. Add in ``project_children``.
    2. Completed (or not)
    3. Sort (if I have the time).
    4. Option to filter by contact - or *all* tickets.
    5. Add a nice *read only* contact header.

    Copying from ``InvoiceListView``...

    """

    paginate_by = 25
    template_name = "crm/contact_ticket_list.html"

    def _contact(self):
        pk = self.kwargs.get("pk")
        contact = get_contact_model().objects.get(pk=pk)
        return contact

    def _default_type_id(self):
        return Ticket.TYPE_DEFAULT

    def _default_status(self):
        return Ticket.STATUS_CURRENT

    def _form(self):
        is_valid = True
        ticket_status_choices = [
            [Ticket.STATUS_ALL, "All Tickets"],
            [Ticket.STATUS_CURRENT, "Open Tickets"],
            [Ticket.STATUS_COMPLETE, "Completed Tickets"],
        ]
        ticket_type_choices = self._ticket_type_choices()
        if self.request.GET:
            form = SearchForm(
                self.request.GET,
                # self.get_initial(),
                ticket_status_choices=ticket_status_choices,
                ticket_type_choices=ticket_type_choices,
            )
            is_valid = form.is_valid()
        else:
            form = SearchForm(
                initial=self.get_initial(),
                ticket_status_choices=ticket_status_choices,
                ticket_type_choices=ticket_type_choices,
            )
        return form, is_valid

    def _status_id(self):
        status_id = self._default_status()
        try:
            status_id = int(self.request.GET.get("ticket_status"))
        except (TypeError, ValueError):
            pass
        return status_id

    def _ticket_type_choices(self):
        result = []
        crm_settings = CrmSettings.load()
        for x in KanbanLane.objects.all().order_by("title"):
            result.append([x.pk, x.title])
        if crm_settings.project_lane:
            result.append([Ticket.TYPE_ALL, "All Tickets"])
            result.append(
                [
                    Ticket.TYPE_PROJECT_CHILDREN,
                    "{} tickets allocated to a project".format(
                        crm_settings.project_lane.title
                    ),
                ]
            )
            result.append(
                [
                    Ticket.TYPE_PROJECT_NO_PARENT,
                    "{} tickets with no project".format(
                        crm_settings.project_lane.title
                    ),
                ]
            )
            result.append(
                [
                    Ticket.TYPE_DEFAULT,
                    "Tickets (excluding tickets inside a {} project)".format(
                        crm_settings.project_lane.title.lower()
                    ),
                ]
            )
        return result

    def _type_id(self):
        type_id = self._default_type_id()
        try:
            type_id = int(self.request.GET.get("ticket_type"))
        except (TypeError, ValueError):
            pass
        return type_id

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        get_parameters = self.request.GET.copy()
        if self.page_kwarg in get_parameters:
            del get_parameters[self.page_kwarg]
        form, is_valid = self._form()
        context.update(
            dict(
                contact=self._contact(),
                form=form,
                get_parameters=get_parameters.urlencode(),
            )
        )
        return context

    def get_initial(self):
        return {
            "ticket_status": self._default_status(),
            "ticket_type": self._default_type_id(),
        }

    def get_queryset(self):
        contact = self._contact()
        status_id = self._status_id()
        type_id = self._type_id()
        # ticket type
        if type_id == Ticket.TYPE_ALL:
            qs = Ticket.objects.tickets(contact)
        elif type_id == Ticket.TYPE_DEFAULT:
            qs = Ticket.objects.excluding_project_children(contact)
        elif type_id == Ticket.TYPE_PROJECT_NO_PARENT:
            qs = Ticket.objects.project_no_parent(contact)
        elif type_id == Ticket.TYPE_PROJECT_CHILDREN:
            qs = Ticket.objects.project_children(contact)
        elif type_id:
            qs = Ticket.objects.tickets(contact)
            qs = qs.filter(kanbancard__column__lane__pk=type_id)
        else:
            qs = Ticket.objects.tickets(contact)
        # status
        if status_id == Ticket.STATUS_COMPLETE:
            qs = qs.exclude(complete__isnull=True)
        elif status_id == Ticket.STATUS_CURRENT:
            qs = qs.filter(complete__isnull=True)
        qs = qs.order_by("-created")
        return qs


class CrmContactUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    model = CrmContact
    form_class = CrmContactForm


class CrmSettingsUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = CrmSettingsForm

    def get_object(self):
        return CrmSettings.load()

    def get_success_url(self):
        return reverse("project.settings")


class HomeTicketListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    paginate_by = 25

    def _display_notify_list(self):
        user_settings = UserSettings.objects.settings(self.request.user)
        return user_settings.display_notify_list

    def _form(self):
        is_valid = True
        if self.request.GET:
            form = TicketListSearchForm(self.request.GET)
            is_valid = form.is_valid()
        else:
            form = TicketListSearchForm(initial=self.get_initial())
        return form, is_valid

    def _page_number(self):
        """Get the page number.

        Code copied from ``django/views/generic/list.py``

        """
        page_number = 1
        page_kwarg = self.page_kwarg
        page = (
            self.kwargs.get(page_kwarg) or self.request.GET.get(page_kwarg) or 1
        )
        try:
            page_number = int(page)
        except ValueError:
            pass
        return page_number

    def _qs_select_related(self, qs):
        """Use select related on a ticket queryset.

        This is the first time I have tried ``select_related``.  It was
        surprisingly easy to make the following improvements by checking the
        SQL output in the Django Debug Toolbar:

        - 141 queries including 133 duplicates
        - 116 queries including 108 duplicates
        - 91 queries including 83 duplicates
        - 75 queries including 65 duplicates
        - 39 queries including 30 duplicates

        """
        return qs.select_related(
            "contact",
            "contact__user",
            "kanbancard",
            "kanbancard__column",
            "kanbancard__column__lane",
            "priority",
        )

    def _user(self):
        result = None
        user_pk = self.request.GET.get("user")
        if user_pk:
            user = get_user_model().objects.get(pk=user_pk)
            if not user == self.request.user:
                result = user
        return result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        display_favourites = display_notify_list = False
        page_number = self._page_number()
        user_assigned = self._user()
        if page_number == 1 and self.request.user.is_staff:
            favourites = Ticket.objects.favourites(
                user_assigned or self.request.user
            ).order_by("contact__user__username", "pk")
            display_favourites = favourites.count()
            if display_favourites:
                favourites = self._qs_select_related(favourites)
            if self._display_notify_list():
                notify_list = (
                    Ticket.objects.notify()
                    .filter(user_assigned=user_assigned or self.request.user)
                    .order_by("due", "-priority__level")
                )
                notify_list = self._qs_select_related(notify_list)
                display_notify_list = notify_list.count()
            else:
                notify_list = Ticket.objects.none()
        else:
            favourites = UserTicketFavourite.objects.none()
            notify_list = Ticket.objects.none()
        # url parameters
        get_parameters = self.request.GET.copy()
        if self.page_kwarg in get_parameters:
            del get_parameters[self.page_kwarg]
        form, _ = self._form()
        context.update(
            dict(
                display_favourites=display_favourites,
                display_notify_list=display_notify_list,
                favourites=favourites,
                form=form,
                get_parameters=get_parameters.urlencode(),
                is_home=True,
                notify_list=notify_list,
                today=date.today(),
                user_assigned=user_assigned,
            )
        )
        return context

    def get_initial(self):
        return {"user": self.request.user.pk}

    def get_queryset(self):
        if self.request.user.is_staff:
            user = self._user() or self.request.user
            qs = Ticket.objects.planner().filter(user_assigned=user)
            if self._display_notify_list():
                qs = qs.order_by("-priority__level", "created")
            else:
                qs = qs.order_by("due", "-priority__level", "created")
            result = self._qs_select_related(qs)
        else:
            result = Ticket.objects.none()
        return result


class IndustryListView(
    LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, ListView
):

    model = Industry
    paginate_by = 20


class IndustryCreateView(
    LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, CreateView
):

    form_class = IndustryForm
    model = Industry

    def get_success_url(self):
        return reverse("crm.industry.list")


class IndustryUpdateView(
    LoginRequiredMixin, SuperuserRequiredMixin, BaseMixin, UpdateView
):

    form_class = IndustryForm
    model = Industry

    def get_success_url(self):
        return reverse("crm.industry.list")


class NoteCreateView(
    LoginRequiredMixin, ContactPermMixin, BaseMixin, CreateView
):
    form_class = NoteForm
    model = Note

    def _ticket(self):
        pk = self.kwargs.get("pk", None)
        ticket = get_object_or_404(Ticket, pk=pk)
        return ticket

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(ticket=self._ticket()))
        return context

    def form_valid(self, form):
        with transaction.atomic():
            self.object = form.save(commit=False)
            self.object.ticket = self._ticket()
            self.object.user = self.request.user
            result = super().form_valid(form)
            transaction.on_commit(
                lambda: update_ticket_index.send_with_options(
                    args=(self.object.ticket.pk,)
                )
            )
        return result

    def test_contact(self):
        ticket = self._ticket()
        return ticket.contact


class NoteUpdateView(
    LoginRequiredMixin, ContactPermMixin, BaseMixin, UpdateView
):
    form_class = NoteForm
    model = Note

    def form_valid(self, form):
        with transaction.atomic():
            result = super().form_valid(form)
            transaction.on_commit(
                lambda: update_ticket_index.send_with_options(
                    args=(self.object.ticket.pk,)
                )
            )
        return result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(ticket=self.object.ticket))
        return context

    def test_contact(self):
        note = self.get_object()
        return note.ticket.contact


class ProjectTicketDueListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    paginate_by = 30
    template_name = "crm/project_ticket_list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(sort_by_due_date=True))
        return context

    def get_queryset(self):
        return Ticket.objects.current().order_by("due", "-priority__level")


class ProjectTicketPriorityListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    paginate_by = 30
    template_name = "crm/project_ticket_list.html"

    def get_queryset(self):
        return Ticket.objects.current().order_by("-priority__level", "due")


class KanbanLaneMixin:
    model = KanbanLane

    def _highlight(self):
        result = None
        value = self.request.GET.get("highlight")
        if value:
            result = int(value)
        return result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        lanes = KanbanLane.objects.lanes().exclude(pk=self.object.pk)
        context.update(
            dict(
                lanes=lanes, user=self.request.user, highlight=self._highlight()
            )
        )
        return context


class KanbanLaneBacklogView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    KanbanLaneMixin,
    BaseMixin,
    DetailView,
):
    template_name = "crm/kanban_lane_backlog.html"


class KanbanLaneBoardView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    KanbanLaneMixin,
    BaseMixin,
    DetailView,
):
    template_name = "crm/kanban_lane_board.html"


class KanbanLaneRedirectView(RedirectView):
    permanent = False
    query_string = False

    def get_redirect_url(self, *args, **kwargs):
        lane = KanbanLane.objects.lanes().first()
        if lane:
            return reverse("crm.kanban.lane.board", args=[lane.pk])
        else:
            raise CrmError("The system does not have any Kanban lanes")


class TicketAPIView(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAdminUser,)

    def get(self, request, pk=None, format=None):
        if pk:
            ticket = Ticket.objects.get(pk=pk)
            serializer = TicketSerializer(ticket)
        else:
            serializer = TicketSerializer(
                Ticket.objects.planner().order_by("pk"), many=True
            )
        return Response(serializer.data)


class TicketChildrenView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, DetailView
):
    model = Ticket
    template_name = "crm/ticket_detail_children.html"

    def _highlight(self):
        result = None
        value = self.request.GET.get("highlight")
        if value:
            result = int(value)
        return result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            dict(highlight=self._highlight(), nodes=self.object.get_family())
        )
        return context


class TicketKanbanCardUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = TicketEmptyForm
    model = Ticket

    operations = {
        "active": {"method": "kanban_active", "message": "made active"},
        "add": {"method": "add_to_backlog", "message": "added to"},
        "board": {
            "method": "move_to_kanban_board",
            "message": "has been moved to the Kanban board from",
        },
        "done": {"method": "kanban_done", "message": "has been marked as done"},
        "down": {
            "method": "backlog_move_down",
            "message": "has been moved down in",
        },
        "next": {
            "method": "kanban_next",
            "message": "has been moved to the next",
        },
        "previous": {
            "method": "kanban_previous",
            "message": "has been moved to the previous",
        },
        "remove": {
            "method": "remove_from_backlog",
            "message": "has been removed from",
        },
        "tracked": {"method": "kanban_tracked", "message": "is being tracked"},
        "up": {"method": "backlog_move_up", "message": "has been moved up in"},
    }

    def _kanban_card(self):
        kanban_card = self.object.kanban_card()
        if not kanban_card:
            raise CrmError(
                "Cannot find Kanban card for ticket {}".format(self.object.pk)
            )
        return kanban_card

    def form_valid(self, form):
        self.object = form.save(commit=False)
        kanban_card = self._kanban_card()
        operation = self.request.POST.get("operation")
        if operation in self.operations:
            item = self.operations[operation]
            message = item["message"]
            method = getattr(kanban_card, item["method"])
            method(self.request.user)
        else:
            raise CrmError(
                "Invalid operation ('{}') for ticket "
                "{}".format(operation, self.object.pk)
            )
        column = kanban_card.column
        message = "Ticket {} {} in the {} column ({})".format(
            self.object.pk, message, column.title, column.lane.title
        )
        messages.info(self.request, message)
        if column.is_backlog():
            url = reverse("crm.kanban.lane.backlog", args=[column.lane.pk])
        else:
            url = reverse("crm.kanban.lane.board", args=[column.lane.pk])
        return HttpResponseRedirect(
            url_with_querystring(url, highlight=self.object.pk)
        )


class TicketMoveConfirmView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    model = Ticket
    form_class = TicketEmptyForm
    template_name = "crm/ticket_form_move_confirm.html"

    def _to_ticket(self):
        pk = self.kwargs["to_pk"]
        to_ticket = Ticket.objects.get(pk=pk)
        return to_ticket

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.parent = self._to_ticket()
        self.object.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(to_ticket=self._to_ticket()))
        return context

    def get_success_url(self):
        pk = self.object.pk
        root = self.object.get_root()
        if root:
            pk = root.pk
        return url_with_querystring(
            reverse("crm.ticket.children", args=[pk]), highlight=self.object.pk
        )


class TicketMoveView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    model = Ticket
    form_class = TicketMoveForm
    template_name = "crm/ticket_form_move.html"

    def form_valid(self, form):
        self.object = form.save(commit=False)
        to_ticket = form.cleaned_data["to_ticket"]
        url = reverse(
            "crm.ticket.move.confirm", args=[self.object.pk, to_ticket]
        )
        return HttpResponseRedirect(url)


class TicketParentClearConfirmView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    model = Ticket
    form_class = TicketEmptyForm
    template_name = "crm/ticket_form_parent_clear_confirm.html"

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.parent = None
        self.object.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse("ticket.detail", args=[self.object.pk])


class TicketToggleCompleteView(
    LoginRequiredMixin, ContactPermMixin, BaseMixin, UpdateView
):
    model = Ticket
    form_class = TicketCompleteForm
    template_name = "crm/ticket_toggle_complete.html"

    def _is_complete(self):
        return bool(self.object.complete)

    def form_valid(self, form):
        self.object = form.save(commit=False)
        with transaction.atomic():
            if self._is_complete():
                self.object.set_uncomplete()
                chat_title = "Un-completed"
                message = "Un-completed ticket {}, {}".format(
                    self.object.number, self.object.title
                )
            else:
                self.object.set_complete(self.request.user)
                chat_title = "Completed"
                message = "Completed ticket {}, {} on {}".format(
                    self.object.number,
                    self.object.title,
                    self.object.complete.strftime("%d/%m/%Y at %H:%M"),
                )
            messages.info(self.request, message)
            self.object.chat(chat_title, self.request.user)
            transaction.on_commit(lambda: send_chat_messages.send())
            result = super().form_valid(form)
        return result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            dict(
                can_complete=self.object.can_complete(),
                is_complete=self._is_complete(),
                kanban_card=self.object.kanban_card(),
            )
        )
        return context

    def get_success_url(self):
        return reverse("ticket.detail", args=[self.object.pk])

    def test_contact(self):
        ticket = self.get_object()
        return ticket.contact


class TicketCreateChildView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, CreateView
):
    form_class = TicketForm
    model = Ticket

    def _display_notify_list(self):
        user_settings = UserSettings.objects.settings(self.request.user)
        return user_settings.display_notify_list

    def _parent_ticket(self):
        pk = self.kwargs["pk"]
        ticket = Ticket.objects.get(pk=pk)
        return ticket

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        parent_ticket = self._parent_ticket()
        context.update(
            dict(contact=parent_ticket.contact, parent=parent_ticket)
        )
        return context

    def form_valid(self, form):
        column = None
        parent_ticket = self._parent_ticket()
        parent_card = parent_ticket.kanban_card()
        if parent_card and parent_card.kanban_first_column():
            # the parent card is in the first column of the Kanban board, so
            # create the child ticket in the same place.
            column = parent_card.column
            state = KanbanCard.DONE
        else:
            kanban_lane = form.cleaned_data.get("ticket_type")
            if kanban_lane:
                column = kanban_lane.backlog_column()
                state = KanbanCard.ACTIVE
        with transaction.atomic():
            self.object = form.save(commit=False)
            self.object.contact = parent_ticket.contact
            self.object.parent = parent_ticket
            self.object.user = self.request.user
            self.object = form.save()
            if column:
                KanbanCard.objects.create_kanban_card(
                    self.object, column, self.request.user, state=state
                )
            transaction.on_commit(
                lambda: update_ticket_index.send_with_options(
                    args=(self.object.pk,)
                )
            )
            self.object.chat("Created", self.request.user)
            transaction.on_commit(lambda: send_chat_messages.send())
        return HttpResponseRedirect(self.get_success_url())

    def get_form_kwargs(self):
        result = super().get_form_kwargs()
        parent_ticket = self._parent_ticket()
        parent_card = parent_ticket.kanban_card()
        if parent_card and parent_card.kanban_first_column():
            can_edit_kanban_column = False
        else:
            can_edit_kanban_column = True
        result.update(
            dict(
                can_edit_kanban_column=can_edit_kanban_column,
                display_due_notify=self._display_notify_list(),
            )
        )
        return result

    def get_success_url(self):
        pk = self.object.pk
        card = self.object.kanban_card()
        if card and card.kanban_first_column():
            url = url_with_querystring(
                reverse("crm.kanban.lane.board", args=[card.column.lane.pk]),
                highlight=self.object.pk,
            )
        else:
            root = self.object.get_root()
            if root:
                pk = root.pk
            url = url_with_querystring(
                reverse("crm.ticket.children", args=[pk]),
                highlight=self.object.pk,
            )
        return url


class TicketCreateView(
    LoginRequiredMixin, ContactPermMixin, BaseMixin, CreateView
):
    form_class = TicketForm
    model = Ticket

    def _contact(self):
        pk = self.kwargs.get("pk")
        contact = get_contact_model().objects.get(pk=pk)
        return contact

    def _display_notify_list(self):
        user_settings = UserSettings.objects.settings(self.request.user)
        return user_settings.display_notify_list

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(contact=self._contact()))
        return context

    def get_form_kwargs(self):
        result = super().get_form_kwargs()
        result.update(
            dict(
                can_edit_kanban_column=True,
                display_due_notify=self._display_notify_list(),
            )
        )
        return result

    def form_valid(self, form):
        column = None
        kanban_lane = form.cleaned_data.get("ticket_type")
        if kanban_lane:
            column = kanban_lane.backlog_column()
        with transaction.atomic():
            self.object = form.save(commit=False)
            self.object.contact = self._contact()
            self.object.user = self.request.user
            self.object = form.save()
            if column:
                KanbanCard.objects.create_kanban_card(
                    self.object, column, self.request.user
                )
            transaction.on_commit(
                lambda: update_ticket_index.send_with_options(
                    args=(self.object.pk,)
                )
            )
            self.object.chat("Created", self.request.user)
            transaction.on_commit(lambda: send_chat_messages.send())
        return HttpResponseRedirect(self.get_success_url())

    def test_contact(self):
        return self._contact()


class TicketDetailMixin:
    paginate_by = 20

    def _ticket(self):
        pk = self.kwargs.get("pk")
        ticket = Ticket.objects.get(pk=pk)
        return ticket

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        ticket = self._ticket()
        quick_chargeable = QuickTimeRecord.objects.quick_chargeable(
            self.request.user
        )
        quick_non_chargeable = QuickTimeRecord.objects.quick_non_chargeable(
            self.request.user
        )
        context.update(
            dict(
                favourite=UserTicketFavourite.objects.is_favourite(
                    self.request.user, ticket
                ),
                kanban_card=ticket.kanban_card,
                quick_chargeable=quick_chargeable,
                quick_non_chargeable=quick_non_chargeable,
                ticket=ticket,
            )
        )
        return context

    def get_queryset(self):
        ticket = self._ticket()
        return ticket_activity(ticket)

    def test_contact(self):
        """For the permission check."""
        return self._ticket().contact


class TicketListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    paginate_by = 20

    def get_queryset(self):
        return Ticket.objects.filter(complete__isnull=True).order_by(
            "due", "-priority__level"
        )


class TicketQuickTimeStartView(
    LoginRequiredMixin, ContactPermMixin, BaseMixin, UpdateView
):
    form_class = TicketEmptyForm
    model = Ticket

    def _quick_time_record(self):
        quick_pk = self.kwargs["quick_pk"]
        try:
            return QuickTimeRecord.objects.get(pk=quick_pk)
        except QuickTimeRecord.DoesNotExist:
            raise CrmError(
                "Cannot find quick time record '{}'".format(quick_pk)
            )

    def form_valid(self, form):
        self.object = form.save(commit=False)
        quick_time_record = self._quick_time_record()
        TimeRecord.objects.start(self.object, quick_time_record)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse("ticket.detail", args=[self.object.pk])

    def test_contact(self):
        ticket = self.get_object()
        return ticket.contact


class TicketUpdateFundingView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = TicketFundingForm
    model = Ticket
    template_name = "crm/ticket_funding_form.html"

    def _funding_changed(self):
        original_ticket = Ticket.objects.get(pk=self.object.pk)
        if original_ticket.funded == self.object.funded:
            result = False
        else:
            result = True
        return result

    def form_valid(self, form):
        self.object = form.save(commit=False)
        funding_changed = self._funding_changed()
        with transaction.atomic():
            result = super().form_valid(form)
            if funding_changed:
                self.object.chat(
                    ":moneybag: Funded by {}".format(self.object.funded.name),
                    self.request.user,
                )
                transaction.on_commit(lambda: send_chat_messages.send())
        return result


class TicketUpdateView(
    LoginRequiredMixin, ContactPermMixin, BaseMixin, UpdateView
):
    form_class = TicketForm
    model = Ticket

    def _display_notify_list(self):
        user_settings = UserSettings.objects.settings(self.request.user)
        return user_settings.display_notify_list

    def _user_assigned_changed(self):
        original_ticket = Ticket.objects.get(pk=self.object.pk)
        if original_ticket.user_assigned == self.object.user_assigned:
            result = False
        else:
            result = True
        return result

    def form_valid(self, form):
        self.object = form.save(commit=False)
        kanban_lane = form.cleaned_data.get("ticket_type")
        user_assigned_changed = self._user_assigned_changed()
        with transaction.atomic():
            if kanban_lane:
                KanbanCard.objects.init_kanban_card(
                    self.object, kanban_lane, self.request.user
                )
            result = super().form_valid(form)
            transaction.on_commit(
                lambda: update_ticket_index.send_with_options(
                    args=(self.object.pk,)
                )
            )
            if user_assigned_changed:
                if self.object.user_assigned:
                    assigned_to = "{} :construction_worker_woman:".format(
                        self.object.user_assigned.username
                    )
                else:
                    assigned_to = "nobody!"
                self.object.chat(
                    "Assigned to {}".format(assigned_to), self.request.user
                )
                transaction.on_commit(lambda: send_chat_messages.send())
        return result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dict(contact=self.object.contact))
        return context

    def get_form_kwargs(self):
        result = super().get_form_kwargs()
        display_due_notify = False
        if self._display_notify_list() or self.object.due_notify:
            display_due_notify = True
        result.update(
            dict(
                can_edit_kanban_column=self.object.can_edit_kanban_column(),
                display_due_notify=display_due_notify,
            )
        )
        return result

    def get_initial(self):
        result = super().get_initial()
        card = self.object.kanban_card()
        if card:
            result.update(dict(ticket_type=card.column.lane))
        return result

    def test_contact(self):
        ticket = self.get_object()
        return ticket.contact


class TicketViewSet(viewsets.ModelViewSet):
    ordering_fields = ("pk",)
    queryset = Ticket.objects.current()
    serializer_class = TicketSerializer

    def get_queryset(self):
        queryset = super().get_queryset()
        contact_pk = self.kwargs.get("contact_pk")
        if contact_pk is not None:
            queryset = queryset.filter(contact_pk=contact_pk)
        return queryset


class UserSettingUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    model = UserSettings
    form_class = UserSettingsForm

    def get_object(self):
        return UserSettings.objects.init_user_settings(self.request.user)

    def get_success_url(self):
        return reverse("project.settings")


class UserTicketFavouriteUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    """Favourite or unfavourite a ticket."""

    model = Ticket
    form_class = TicketEmptyForm
    template_name = "crm/ticket_favourite_form.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            dict(
                favourite=UserTicketFavourite.objects.is_favourite(
                    self.request.user, self.object
                )
            )
        )
        return context

    def get_success_url(self):
        return reverse("ticket.detail", args=[self.object.pk])


class UserTicketFavouriteView(UserTicketFavouriteUpdateView):
    def form_valid(self, form):
        self.object = form.save(commit=False)
        UserTicketFavourite.objects.favourite(self.request.user, self.object)
        return HttpResponseRedirect(self.get_success_url())


class UserTicketUnFavouriteView(UserTicketFavouriteUpdateView):
    def form_valid(self, form):
        self.object = form.save(commit=False)
        UserTicketFavourite.objects.unfavourite(self.request.user, self.object)
        return HttpResponseRedirect(self.get_success_url())
