# -*- encoding: utf-8 -*-
from django import forms
from django.contrib.auth import get_user_model

from base.form_utils import RequiredFieldForm
from .models import (
    CrmContact,
    CrmSettings,
    Industry,
    KanbanLane,
    Note,
    Ticket,
    UserSettings,
)


class UserModelChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, instance):
        full_name = instance.get_full_name() or instance.username
        return f"{full_name}"


class CrmContactForm(forms.ModelForm):
    class Meta:
        model = CrmContact
        fields = ("industry",)


class CrmSettingsForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        f = self.fields["project_lane"]
        f.widget.attrs.update({"class": "chosen-select"})

    class Meta:
        model = CrmSettings
        fields = ("project_lane",)


class IndustryForm(forms.ModelForm):
    class Meta:
        model = Industry
        fields = ("name",)


class NoteForm(RequiredFieldForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["title"].widget.attrs.update({"class": "pure-input-1"})
        self.fields["description"].widget.attrs.update(
            {"class": "pure-input-1", "rows": 10}
        )

    class Meta:
        model = Note
        fields = ("title", "description")


class ReportParametersKanbanLaneForm(forms.Form):
    kanban_lane = forms.ModelChoiceField(
        label="Lane",
        queryset=KanbanLane.objects.none(),
        required=True,
        widget=forms.Select(
            attrs={
                "class": "chosen-select pure-u-1 pure-u-md-1-3",
                "data-placeholder": "Select a Kanban lane",
            }
        ),
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        f = self.fields["kanban_lane"]
        f.queryset = KanbanLane.objects.all().order_by("title")

    def clean(self, *args, **kwargs):
        cleaned_data = super().clean()
        kanban_lane = cleaned_data.get("kanban_lane")
        if kanban_lane:
            cleaned_data["kanban_lane"] = kanban_lane.title
        return cleaned_data


class SearchForm(forms.Form):
    ticket_status = forms.ChoiceField(choices=[], label="", required=True)
    ticket_type = forms.ChoiceField(choices=[], label="", required=True)

    def __init__(self, *args, **kwargs):
        ticket_status_choices = kwargs.pop("ticket_status_choices")
        ticket_type_choices = kwargs.pop("ticket_type_choices")
        super().__init__(*args, **kwargs)
        # status
        f = self.fields["ticket_status"]
        f.choices = ticket_status_choices
        f.widget.attrs.update({"class": "chosen-select"})
        # ticket types
        f = self.fields["ticket_type"]
        f.choices = ticket_type_choices
        f.widget.attrs.update({"class": "chosen-select"})


class TicketCompleteForm(forms.ModelForm):
    class Meta:
        model = Ticket
        fields = ()

    def clean(self):
        super().clean()
        if self.instance.can_complete():
            pass
        else:
            raise forms.ValidationError(
                "The ticket cannot be completed.  Is it on a Kanban board?",
                code="ticket__can_complete__not",
            )


class TicketEmptyForm(forms.ModelForm):
    class Meta:
        model = Ticket
        fields = ()


class TicketListSearchForm(forms.Form):
    user = UserModelChoiceField(
        queryset=get_user_model().objects.none(), label="", required=True
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        f = self.fields["user"]
        f.queryset = (
            get_user_model()
            .objects.filter(is_active=True, is_staff=True)
            .order_by("username")
        )
        f.widget.attrs.update({"class": "chosen-select"})


class TicketMoveForm(forms.ModelForm):
    to_ticket = forms.IntegerField()

    class Meta:
        model = Ticket
        fields = ()

    def clean_to_ticket(self):
        data = self.cleaned_data.get("to_ticket")
        try:
            Ticket.objects.get(pk=data)
            # if ticket.complete:
            #     raise forms.ValidationError(
            #         "Ticket '{}' is complete".format(data),
            #         code='to_ticket__is_complete',
            #     )
        except Ticket.DoesNotExist:
            raise forms.ValidationError(
                "Ticket '{}' does not exist".format(data),
                code="to_ticket__does_not_exist",
            )
        return data


class TicketForm(RequiredFieldForm):
    ticket_type = forms.ModelChoiceField(
        label="Ticket Type", queryset=KanbanLane.objects.none(), required=False
    )

    def __init__(self, *args, **kwargs):
        can_edit_kanban_column = kwargs.pop("can_edit_kanban_column")
        display_due_notify = kwargs.pop("display_due_notify")
        super().__init__(*args, **kwargs)
        self.fields["description"].widget.attrs.update(
            {"class": "pure-input-1", "rows": 10}
        )
        self.fields["title"].widget.attrs.update({"class": "pure-input-1"})
        qs = (
            get_user_model()
            .objects.filter(is_active=True, is_staff=True)
            .order_by("username")
        )
        f = self.fields["user_assigned"]
        f.queryset = qs
        f.label_from_instance = user_label_from_instance
        if not display_due_notify:
            del self.fields["due_notify"]
        if can_edit_kanban_column:
            f = self.fields["ticket_type"]
            f.queryset = KanbanLane.objects.all().order_by("title")
            # f.widget.attrs.update({'class': 'chosen-select pure-input-1-2'})
        else:
            del self.fields["ticket_type"]

    class Meta:
        model = Ticket
        fields = (
            "priority",
            "title",
            "description",
            "user_assigned",
            "ticket_type",
            "due_notify",
            "due",
            # "funded",
        )

    def clean(self):
        cleaned_data = super().clean()
        due = cleaned_data.get("due")
        due_notify = cleaned_data.get("due_notify")
        if due:
            if due_notify and due_notify > due:
                raise forms.ValidationError(
                    (
                        "The notification date for the due date must "
                        "be before the due date."
                    ),
                    code="ticket__due_notify__too_late",
                )
        elif due_notify:
            raise forms.ValidationError(
                "You have a notification date, please add a due date",
                code="ticket__empty_due_with_notify",
            )


class TicketFundingForm(RequiredFieldForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        f = self.fields["funded"]
        f.required = True
        f.widget.attrs.update({"class": "chosen-select pure-input-1-2"})

    class Meta:
        model = Ticket
        fields = ("funded",)


class UserSettingsForm(forms.ModelForm):
    class Meta:
        model = UserSettings
        fields = ("display_notify_list",)


def user_label_from_instance(obj):
    result = obj.get_full_name()
    if not result:
        result = obj.username
    return result
