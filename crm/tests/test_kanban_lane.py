# -*- encoding: utf-8 -*-
import pytest

from crm.models import CrmError, KanbanLane
from crm.tests.factories import (
    KanbanCardFactory,
    KanbanColumnFactory,
    KanbanLaneFactory,
    TicketFactory,
)
from login.tests.factories import UserFactory


@pytest.mark.django_db
def test_backlog():
    """Backlog tickets for this Kanban lane."""
    lane = KanbanLaneFactory()
    backlog_column = KanbanColumnFactory(lane=lane, order=0, title="Backlog")
    specify_column = KanbanColumnFactory(lane=lane, order=1, title="Specify")
    # pending backlog
    t1 = TicketFactory(title="t1")
    KanbanCardFactory(ticket=t1, column=backlog_column, order=0)
    # specify column
    t2 = TicketFactory(title="t2")
    KanbanCardFactory(ticket=t2, column=specify_column, order=1)
    # backlog
    t3 = TicketFactory(title="t3")
    KanbanCardFactory(ticket=t3, column=backlog_column, order=1)
    # complete
    t4 = TicketFactory(title="t4")
    KanbanCardFactory(ticket=t4, column=backlog_column, order=1)
    t4.set_complete(UserFactory())
    t4.save()
    assert ["t3"] == [x.title for x in lane.backlog()]


@pytest.mark.django_db
def test_backlog_column():
    lane = KanbanLaneFactory()
    backlog_column = KanbanColumnFactory(lane=lane, order=0, title="Backlog")
    KanbanColumnFactory(lane=lane, order=1, title="Specify")
    KanbanColumnFactory(lane=lane, order=2, title="Implement")
    assert backlog_column == lane.backlog_column()


@pytest.mark.django_db
def test_backlog_column_not():
    """A Kanban lane must have a column."""
    lane = KanbanLaneFactory(title="Support")
    with pytest.raises(CrmError) as e:
        lane.backlog_column()
    assert "Kanban lane 'Support' has no backlog column" in str(e.value)


@pytest.mark.django_db
def test_backlog_pending():
    """Tickets which can be added to the backlog for this Kanban lane."""
    lane = KanbanLaneFactory()
    backlog_column = KanbanColumnFactory(lane=lane, order=0, title="Backlog")
    specify_column = KanbanColumnFactory(lane=lane, order=1, title="Specify")
    # pending backlog
    t1 = TicketFactory(title="t1")
    KanbanCardFactory(ticket=t1, column=backlog_column, order=0)
    # specify column
    t2 = TicketFactory(title="t2")
    KanbanCardFactory(ticket=t2, column=specify_column, order=1)
    # backlog
    t3 = TicketFactory(title="t3")
    KanbanCardFactory(ticket=t3, column=backlog_column, order=1)
    # complete
    t4 = TicketFactory(title="t4")
    KanbanCardFactory(ticket=t4, column=backlog_column, order=1)
    t4.set_complete(UserFactory())
    t4.save()
    assert ["t1"] == [x.title for x in lane.backlog_pending()]


@pytest.mark.django_db
def test_columns():
    lane = KanbanLaneFactory(title="Development", order=1)
    KanbanColumnFactory(lane=lane, order=0, title="Backlog")
    KanbanColumnFactory(lane=lane, order=1, title="Specify")
    KanbanColumnFactory(lane=lane, order=2, title="Implement")
    assert ["Specify", "Implement"] == [x.title for x in lane.columns()]


@pytest.mark.django_db
def test_create_kanban_lane():
    lane = KanbanLane.objects.create_kanban_lane("Development", 1)
    assert 1 == lane.order
    assert lane.funded is False
    assert "Development" == lane.title


@pytest.mark.django_db
def test_create_kanban_lane_funded():
    lane = KanbanLane.objects.create_kanban_lane("Development", 1, True)
    assert 1 == lane.order
    assert lane.funded is True
    assert "Development" == lane.title


@pytest.mark.django_db
def test_first_column():
    lane = KanbanLaneFactory()
    KanbanColumnFactory(lane=lane, order=0, title="Backlog")
    column_1 = KanbanColumnFactory(lane=lane, order=1, title="Specify")
    KanbanColumnFactory(lane=lane, order=2, title="Implement")
    assert column_1 == lane.first_column()


@pytest.mark.django_db
def test_first_column_not():
    """A Kanban lane must have a column."""
    lane = KanbanLaneFactory(title="Support")
    with pytest.raises(CrmError) as e:
        lane.first_column()
    assert "Kanban lane 'Support' has no visible columns" in str(e.value)


@pytest.mark.django_db
def test_first_column_only_backlog():
    """A Kanban lane must have a column.

    .. note:: The backlog column (order ``0``) is not displayed on the Kanban
              board, so is not returned as the first column.

    """
    lane = KanbanLaneFactory(title="Support")
    KanbanColumnFactory(lane=lane, order=0, title="Backlog")
    with pytest.raises(CrmError) as e:
        lane.first_column()
    assert "Kanban lane 'Support' has no visible columns" in str(e.value)


@pytest.mark.django_db
def test_init_kanban_lane():
    lane = KanbanLane.objects.init_kanban_lane("Development", 1)
    assert 1 == lane.order
    assert lane.funded is False
    assert "Development" == lane.title


@pytest.mark.django_db
def test_init_kanban_lane_funded():
    lane = KanbanLane.objects.init_kanban_lane("Development", 1, True)
    assert 1 == lane.order
    assert lane.funded is True
    assert "Development" == lane.title


@pytest.mark.django_db
def test_init_kanban_lane_update():
    lane = KanbanLane.objects.init_kanban_lane("Development", 0)
    assert 0 == lane.order
    lane = KanbanLane.objects.init_kanban_lane("Development", 1)
    lane.refresh_from_db()
    assert 1 == lane.order


@pytest.mark.django_db
def test_lanes():
    KanbanLaneFactory(title="Ideas", order=0)
    KanbanLaneFactory(title="Development", order=1)
    KanbanLaneFactory(title="Support", order=2)
    assert ["Development", "Support"] == [
        x.title for x in KanbanLane.objects.lanes()
    ]


@pytest.mark.django_db
def test_str():
    assert "Support" == str(KanbanLaneFactory(title="Support", funded=True))
