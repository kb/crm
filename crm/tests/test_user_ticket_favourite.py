# -*- encoding: utf-8 -*-
import pytest

from crm.models import UserTicketFavourite
from login.tests.factories import UserFactory
from .factories import TicketFactory, UserTicketFavouriteFactory


@pytest.mark.django_db
def test_init_user_ticket_favourite():
    user = UserFactory()
    ticket = TicketFactory()
    assert UserTicketFavourite.objects.is_favourite(user, ticket) is False
    UserTicketFavourite.objects.init_user_ticket_favourite(user, ticket)
    assert UserTicketFavourite.objects.is_favourite(user, ticket) is True


@pytest.mark.django_db
def test_is_favourite():
    user = UserFactory()
    ticket = TicketFactory()
    UserTicketFavourite.objects.favourite(user, ticket)
    assert UserTicketFavourite.objects.is_favourite(user, ticket) is True


@pytest.mark.django_db
def test_is_favourite_deleted():
    user = UserFactory()
    ticket = TicketFactory()
    UserTicketFavourite.objects.favourite(user, ticket)
    obj = UserTicketFavourite.objects.get(user=user, ticket=ticket)
    obj.set_deleted(user)
    assert UserTicketFavourite.objects.is_favourite(user, ticket) is False


@pytest.mark.django_db
def test_is_favourite_not():
    user = UserFactory()
    ticket = TicketFactory()
    assert UserTicketFavourite.objects.is_favourite(user, ticket) is False


@pytest.mark.django_db
def test_str():
    user = UserFactory(username="patrick")
    ticket = TicketFactory(title="Apple")
    obj = UserTicketFavouriteFactory(user=user, ticket=ticket)
    assert "patrick {} Apple".format(ticket.pk) == str(obj)


@pytest.mark.django_db
def test_unfavourite():
    user = UserFactory()
    ticket = TicketFactory()
    UserTicketFavourite.objects.unfavourite(user, ticket)
    assert UserTicketFavourite.objects.is_favourite(user, ticket) is False
