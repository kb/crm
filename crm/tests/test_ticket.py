# -*- encoding: utf-8 -*-
import pytest

from datetime import date
from dateutil.relativedelta import relativedelta
from django.core.management import call_command
from django.utils import timezone

from contact.tests.factories import ContactFactory
from crm.models import (
    Funded,
    KanbanCard,
    KanbanColumn,
    Ticket,
    UserTicketFavourite,
)
from crm.tests.factories import (
    CrmSettingsFactory,
    KanbanCardFactory,
    KanbanColumnFactory,
    KanbanLaneFactory,
    PriorityFactory,
    TicketFactory,
)
from login.tests.factories import UserFactory
from search.tests.helper import check_search_methods


@pytest.mark.django_db
def test_can_complete():
    """Ticket is not on the Kanban board, so can be completed."""
    ticket = TicketFactory()
    assert ticket.can_complete() is True


@pytest.mark.django_db
def test_can_complete_not():
    """Ticket is on the Kanban board, so cannot be completed."""
    ticket = TicketFactory()
    lane = KanbanLaneFactory(order=1)
    column = KanbanColumnFactory(lane=lane, order=1)
    KanbanColumnFactory(lane=lane, order=2)
    KanbanCardFactory(ticket=ticket, column=column, state=KanbanCard.ACTIVE)
    assert ticket.can_complete() is False


@pytest.mark.django_db
def test_can_edit_kanban_column():
    ticket = TicketFactory()
    column = KanbanColumnFactory(lane=KanbanLaneFactory(order=0), order=1)
    KanbanCardFactory(ticket=ticket, column=column)
    assert ticket.can_edit_kanban_column() is True


@pytest.mark.django_db
def test_can_edit_kanban_column_no_kanban_column():
    ticket = TicketFactory()
    assert ticket.can_edit_kanban_column() is True


@pytest.mark.django_db
def test_can_edit_kanban_column_on_backlog():
    ticket = TicketFactory()
    column = KanbanColumnFactory(lane=KanbanLaneFactory(order=1), order=0)
    KanbanCardFactory(ticket=ticket, column=column)
    assert ticket.can_edit_kanban_column() is True


@pytest.mark.django_db
def test_can_edit_kanban_column_on_kanban_board():
    ticket = TicketFactory()
    column = KanbanColumnFactory(lane=KanbanLaneFactory(order=1), order=1)
    KanbanCardFactory(ticket=ticket, column=column)
    assert ticket.can_edit_kanban_column() is False


@pytest.mark.django_db
def test_completed():
    TicketFactory(title="t1", complete=timezone.now())
    TicketFactory(title="t2")
    TicketFactory(title="t3", complete=timezone.now(), deleted=True)
    TicketFactory(title="t4", complete=timezone.now())
    assert ["t4", "t1"] == [
        x.title for x in Ticket.objects.completed().order_by("-complete")
    ]


@pytest.mark.django_db
def test_current():
    TicketFactory(title="t1")
    TicketFactory(title="t2", deleted=True)
    TicketFactory(complete=timezone.now(), title="t3")
    TicketFactory(title="t4")
    qs = Ticket.objects.current().order_by("title")
    assert ["t1", "t4"] == [obj.title for obj in qs]


@pytest.mark.django_db
def test_due():
    ticket = TicketFactory(due=date.today())
    assert not ticket.is_overdue


@pytest.mark.django_db
def test_excluding_project_children():
    """A list of tickets and top level MPTT parent tickets."""
    sales_lane = KanbanLaneFactory(title="Sales")
    KanbanColumnFactory(lane=sales_lane, order=0, title="Backlog")
    development_lane = KanbanLaneFactory(title="Development")
    KanbanColumnFactory(lane=development_lane, order=0, title="Backlog")
    support_lane = KanbanLaneFactory(title="Support")
    KanbanColumnFactory(lane=support_lane, order=0, title="Backlog")
    user = UserFactory()
    contact = ContactFactory()
    # settings
    CrmSettingsFactory(project_lane=development_lane)
    # development ticket (and parent)
    t1 = TicketFactory(title="a", contact=contact)
    KanbanCard.objects.init_kanban_card(t1, development_lane, user)
    # development ticket (and child)
    t2 = TicketFactory(title="b", contact=contact, parent=t1)
    KanbanCard.objects.init_kanban_card(t2, development_lane, user)
    # support ticket
    t3 = TicketFactory(title="c", contact=contact)
    KanbanCard.objects.init_kanban_card(t3, support_lane, user)
    # development ticket
    t4 = TicketFactory(title="d", contact=contact)
    KanbanCard.objects.init_kanban_card(t4, development_lane, user)
    # another contact
    t5 = TicketFactory(title="e", contact=ContactFactory())
    KanbanCard.objects.init_kanban_card(t5, support_lane, user)
    # sales ticket
    t6 = TicketFactory(title="f", contact=contact)
    KanbanCard.objects.init_kanban_card(t6, sales_lane, user)
    # test
    qs = Ticket.objects.excluding_project_children(contact)
    assert ["a", "c", "d", "f"] == [x.title for x in qs]
    # test 'project_children'
    qs = Ticket.objects.project_children(contact)
    assert ["b"] == [x.title for x in qs]


@pytest.mark.django_db
def test_favourites():
    u1 = UserFactory()
    u2 = UserFactory()
    t1 = TicketFactory(title="t1")
    TicketFactory(title="t2")
    t3 = TicketFactory(title="t3")
    t4 = TicketFactory(title="t4", deleted=True)
    t5 = TicketFactory(title="t5")
    UserTicketFavourite.objects.favourite(u1, t1)
    UserTicketFavourite.objects.favourite(u2, t1)
    UserTicketFavourite.objects.favourite(u1, t3)
    UserTicketFavourite.objects.favourite(u1, t4)
    UserTicketFavourite.objects.favourite(u1, t5)
    # delete favourite for ticket 3
    obj = UserTicketFavourite.objects.get(user=u1, ticket=t3)
    obj.set_deleted(u1)
    qs = Ticket.objects.favourites(u1)
    assert ["t1", "t5"] == [x.title for x in qs]


@pytest.mark.django_db
def test_is_deleted():
    x = TicketFactory()
    x.set_complete(UserFactory())
    assert x.is_deleted is False


@pytest.mark.django_db
def test_is_deleted_not():
    x = TicketFactory()
    assert x.is_deleted is False


@pytest.mark.django_db
def test_is_parent():
    t1 = TicketFactory()
    t2 = TicketFactory(parent=t1)
    t3 = TicketFactory()
    assert t1.is_parent() is True
    assert t2.is_parent() is False
    assert t3.is_parent() is False


@pytest.mark.django_db
def test_level_range():
    t1 = TicketFactory()
    t2 = TicketFactory(parent=t1)
    t3 = TicketFactory(parent=t2)
    assert range(0, 0) == t1.level_range
    assert range(0, 1) == t2.level_range
    assert range(0, 2) == t3.level_range


@pytest.mark.parametrize(
    "lane_title,funded_slug,expect",
    [
        ("Admin", Funded.FREE_OF_CHARGE, False),
        ("Admin", Funded.SALES_ORDER, False),
        ("Admin", None, False),
        ("Development", Funded.FREE_OF_CHARGE, False),
        ("Development", Funded.SALES_ORDER, True),
        ("Development", None, False),
        ("Sales", Funded.FREE_OF_CHARGE, False),
        ("Sales", Funded.SALES_ORDER, False),
        ("Sales", None, False),
        ("Support", Funded.FREE_OF_CHARGE, False),
        ("Support", Funded.SALES_ORDER, True),
        ("Support", None, False),
    ],
)
@pytest.mark.django_db
def test_needs_sales_order(lane_title, funded_slug, expect):
    """Tickets for development and support need a sales order when not FOC."""
    call_command("init-app-crm")
    # get the first column in the lane
    column = KanbanColumn.objects.filter(lane__title=lane_title).first()
    ticket = TicketFactory()
    if funded_slug:
        funded = Funded.objects.get(slug=funded_slug)
        ticket.funded = funded
        ticket.save()
    card = KanbanCardFactory(
        ticket=ticket, column=column, state=KanbanCard.ACTIVE
    )
    assert card.ticket.needs_sales_order() is expect


@pytest.mark.django_db
def test_notify():
    minus_20_days = date.today() + relativedelta(days=-20)
    date.today() + relativedelta(days=-40)
    plus_20_days = date.today() + relativedelta(days=+20)
    plus_40_days = date.today() + relativedelta(days=+20)
    # due in 20 days, notify 20 days ago
    TicketFactory(due=plus_20_days, due_notify=minus_20_days, title="t1")
    # due in 40 days, notify in 20 days
    TicketFactory(due=plus_40_days, due_notify=plus_20_days, title="t2")
    # due in 20 days, notify today
    TicketFactory(due=plus_20_days, due_notify=date.today(), title="t3")
    # complete - so ignore
    TicketFactory(
        due=plus_20_days,
        due_notify=minus_20_days,
        title="t4",
        complete=timezone.now(),
    )
    # delete - so ignore
    TicketFactory(due=plus_20_days, due_notify=None, title="t5", deleted=True)
    # due in 20 days
    TicketFactory(due=plus_20_days, due_notify=None, title="t6")
    # test
    qs = Ticket.objects.notify().order_by("title")
    assert ["t1", "t3", "t6"] == [obj.title for obj in qs]


@pytest.mark.django_db
def test_overdue():
    ticket = TicketFactory(due=date(2010, 1, 1))
    assert ticket.is_overdue


@pytest.mark.django_db
def test_parent_contact():
    contact = ContactFactory()
    t1 = TicketFactory(contact=contact)
    t2 = TicketFactory(parent=t1)
    t3 = TicketFactory()
    assert contact == t2.parent_contact()
    assert t3.parent_contact() is None


@pytest.mark.django_db
def test_planner():
    priority_0 = PriorityFactory(level=0)
    priority_1 = PriorityFactory(level=1)
    TicketFactory(priority=priority_1, title="t1")
    TicketFactory(priority=priority_1, title="t2", complete=timezone.now())
    TicketFactory(priority=priority_1, title="t3", deleted=True)
    TicketFactory(priority=priority_0, title="t4")
    TicketFactory(priority=priority_1, title="t5")
    qs = Ticket.objects.planner().order_by("title")
    assert ["t1", "t5"] == [obj.title for obj in qs]


@pytest.mark.django_db
def test_search_methods():
    check_search_methods(TicketFactory())


@pytest.mark.django_db
def test_str():
    str(TicketFactory())


@pytest.mark.django_db
def test_tickets_with_contact():
    contact = ContactFactory()
    TicketFactory(contact=contact, due=date.today(), title="t1")
    TicketFactory(contact=contact, title="t2")
    TicketFactory(contact=contact, title="t3", deleted=True)
    TicketFactory(contact=contact, complete=timezone.now(), title="t4")
    TicketFactory(title="t5")
    qs = Ticket.objects.tickets(contact).order_by("title")
    assert ["t1", "t2", "t4"] == [obj.title for obj in qs]
