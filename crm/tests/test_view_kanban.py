# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse

from base.url_utils import url_with_querystring
from chat.models import Channel
from chat.tests.factories import ChannelFactory
from contact.tests.factories import ContactFactory
from crm.models import CrmError, KanbanCard, Ticket
from crm.tests.factories import (
    KanbanCardFactory,
    KanbanColumnFactory,
    KanbanLaneFactory,
    PriorityFactory,
    TicketFactory,
)
from login.tests.factories import TEST_PASSWORD, UserFactory


@pytest.mark.django_db
def test_ticket_create_child_on_kanban_first_column(client):
    """The ticket is in the 'Specify' column, so the child will be as well."""
    ChannelFactory(slug=Channel.ACTIVITY)
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    contact = ContactFactory()
    parent_ticket = TicketFactory(contact=contact)
    lane = KanbanLaneFactory()
    KanbanColumnFactory(lane=lane, order=0, title="Backlog")
    specify_column = KanbanColumnFactory(lane=lane, order=1, title="Specify")
    KanbanColumnFactory(lane=lane, order=2, title="Implement")
    card = KanbanCardFactory(ticket=parent_ticket, column=specify_column)
    assert parent_ticket.children.count() == 0
    priority = PriorityFactory()
    url = reverse("crm.ticket.create.child", kwargs={"pk": parent_ticket.pk})
    data = {"priority": priority.pk, "title": "Apple Juice"}
    response = client.post(url, data)
    assert 302 == response.status_code, response.context["form"].errors
    ticket = Ticket.objects.get(title="Apple Juice")
    card = ticket.kanban_card()
    assert card is not None
    assert card.column == specify_column
    assert card.state == KanbanCard.DONE
    url = reverse("crm.kanban.lane.board", args=[card.column.lane.pk])
    expect = url_with_querystring(url, highlight=ticket.pk)
    assert expect == response["Location"]


@pytest.mark.django_db
def test_ticket_create_child_on_kanban_hide_ticket_type(client):
    """Hide the ticket type if the parent ticket is on the first column."""
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    PriorityFactory()
    contact = ContactFactory()
    parent_ticket = TicketFactory(contact=contact)
    column = KanbanColumnFactory(lane=KanbanLaneFactory(order=1), order=1)
    KanbanCardFactory(ticket=parent_ticket, column=column)
    url = reverse("crm.ticket.create.child", kwargs={"pk": parent_ticket.pk})
    response = client.get(url)
    assert 200 == response.status_code
    form = response.context["form"]
    assert "ticket_type" not in form.fields, list(form.fields.keys())


@pytest.mark.parametrize(
    "operation,on_board,order,state,redirect_to_board",
    [
        ("active", True, 0, KanbanCard.ACTIVE, True),
        ("add", False, 0, KanbanCard.ACTIVE, False),
        ("board", False, 8, KanbanCard.ACTIVE, True),
        ("done", True, 0, KanbanCard.ACTIVE, True),
        ("down", True, 0, KanbanCard.ACTIVE, True),
        ("next", True, 0, KanbanCard.DONE, True),
        ("previous", True, 0, KanbanCard.ACTIVE, False),
        ("remove", False, 8, KanbanCard.ACTIVE, False),
        ("tracked", True, 0, KanbanCard.ACTIVE, True),
        ("up", True, 8, KanbanCard.ACTIVE, True),
    ],
)
@pytest.mark.django_db
def test_ticket_kanban_card_update(
    client, operation, on_board, order, state, redirect_to_board
):
    """Try all the operations in the view.

    We are just testing the happy path here, so when we add a new operation,
    adjust the parameters so the test passes.

    """
    user = UserFactory(is_staff=True)
    # login
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    # setup
    lane = KanbanLaneFactory()
    ticket = TicketFactory()
    backlog_column = KanbanColumnFactory(lane=lane, order=0, title="Backlog")
    specify_column = KanbanColumnFactory(lane=lane, order=1, title="Specify")
    KanbanColumnFactory(lane=lane, order=2, title="Development")
    if on_board:
        column = specify_column
    else:
        column = backlog_column
    KanbanCardFactory(ticket=ticket, column=column, order=order, state=state)
    # test
    url = reverse("crm.ticket.kanban.card.update", args=[ticket.pk])
    data = {"operation": operation}
    response = client.post(url, data)
    assert 302 == response.status_code
    if redirect_to_board:
        url = reverse("crm.kanban.lane.board", args=[lane.pk])
    else:
        url = reverse("crm.kanban.lane.backlog", args=[lane.pk])
    expect = url_with_querystring(url, highlight=ticket.pk)
    assert expect == response["Location"]


@pytest.mark.django_db
def test_ticket_kanban_card_update_add_to_backlog(client):
    user = UserFactory(is_staff=True)
    # login
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    # setup
    lane = KanbanLaneFactory()
    ticket = TicketFactory()
    backlog_column = KanbanColumnFactory(lane=lane, order=0, title="Backlog")
    card = KanbanCardFactory(ticket=ticket, column=backlog_column, order=0)
    KanbanCardFactory(column=backlog_column, order=99)
    assert card.order == 0
    # test
    url = reverse("crm.ticket.kanban.card.update", args=[ticket.pk])
    data = {"operation": "add"}
    response = client.post(url, data)
    assert 302 == response.status_code
    expect = url_with_querystring(
        reverse("crm.kanban.lane.backlog", args=[lane.pk]), highlight=ticket.pk
    )
    assert expect == response["Location"]
    card.refresh_from_db()
    assert card.order == 1


@pytest.mark.django_db
def test_ticket_kanban_card_update_remove_from_backlog(client):
    user = UserFactory(is_staff=True)
    # login
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    # setup
    lane = KanbanLaneFactory()
    ticket = TicketFactory()
    backlog_column = KanbanColumnFactory(lane=lane, order=0, title="Backlog")
    card = KanbanCardFactory(ticket=ticket, column=backlog_column, order=99)
    # test
    url = reverse("crm.ticket.kanban.card.update", args=[ticket.pk])
    data = {"operation": "remove"}
    response = client.post(url, data)
    assert 302 == response.status_code
    expect = url_with_querystring(
        reverse("crm.kanban.lane.backlog", args=[lane.pk]), highlight=ticket.pk
    )
    assert expect == response["Location"]
    card.refresh_from_db()
    assert card.order == 0


@pytest.mark.django_db
def test_ticket_kanban_card_update_invalid_operation(client):
    user = UserFactory(is_staff=True)
    # login
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    # setup
    lane = KanbanLaneFactory()
    ticket = TicketFactory()
    backlog_column = KanbanColumnFactory(lane=lane, order=0, title="Backlog")
    KanbanCardFactory(ticket=ticket, column=backlog_column, order=99)
    # test
    url = reverse("crm.ticket.kanban.card.update", args=[ticket.pk])
    data = {"operation": "does-not-exist"}
    with pytest.raises(CrmError) as e:
        client.post(url, data)
    assert "Invalid operation ('does-not-exist') for ticket" in str(e.value)


@pytest.mark.django_db
def test_ticket_update_ticket_type_hide(client):
    """Hide the ticket type field if the ticket is on a Kanban board."""
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    PriorityFactory()
    ticket = TicketFactory()
    column = KanbanColumnFactory(lane=KanbanLaneFactory(order=1), order=1)
    KanbanCardFactory(ticket=ticket, column=column)
    url = reverse("crm.ticket.update", kwargs={"pk": ticket.pk})
    response = client.get(url)
    assert 200 == response.status_code
    form = response.context["form"]
    assert "ticket_type" not in form.fields, list(form.fields.keys())


@pytest.mark.django_db
def test_ticket_update_ticket_type_show(client):
    """Show the ticket type if the ticket is not on a Kanban board.

    .. note:: The ticket type is a foreign key field to the Kanban Lane model.

    """

    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    PriorityFactory()
    ticket = TicketFactory()
    kanban_lane = KanbanLaneFactory(title="Development", order=1)
    column = KanbanColumnFactory(lane=kanban_lane, order=0)
    KanbanCardFactory(ticket=ticket, column=column)
    url = reverse("crm.ticket.update", kwargs={"pk": ticket.pk})
    response = client.get(url)
    assert 200 == response.status_code
    form = response.context["form"]
    assert "ticket_type" in form.fields, list(form.fields.keys())
    assert kanban_lane == form.initial["ticket_type"]


@pytest.mark.django_db
def test_ticket_update_ticket_type_update(client):
    """Can change the ticket type if the ticket is not on a Kanban board."""
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    lane = KanbanLaneFactory(title="Development", order=1)
    # this is the backlog column (so not displayed on the Kanban board)
    backlog = KanbanColumnFactory(title="Backlog", order=0, lane=lane)
    ticket = TicketFactory()
    # column = KanbanColumnFactory(
    #     title='Validate',
    #     order=0,
    #     lane=KanbanLaneFactory(title='Support', order=1),
    # )
    # KanbanCardFactory(ticket=ticket, column=column)
    assert ticket.kanban_card() is None
    url = reverse("crm.ticket.update", kwargs={"pk": ticket.pk})
    data = {
        "priority": PriorityFactory().pk,
        "title": "Orange Juice",
        "ticket_type": lane.pk,
    }
    response = client.post(url, data)
    assert 302 == response.status_code
    expect = reverse("ticket.detail", args=[ticket.pk])
    assert expect == response["Location"]
    ticket.refresh_from_db()
    # card = ticket.kanban_card()
    card = KanbanCard.objects.get(ticket=ticket)
    assert card is not None
    assert backlog == card.column
    assert lane == card.column.lane


@pytest.mark.django_db
def test_ticket_update_ticket_type_update_change(client):
    """Can change the ticket type if the ticket is not on a Kanban board."""
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    lane = KanbanLaneFactory(title="Development", order=1)
    backlog = KanbanColumnFactory(title="Backlog", order=0, lane=lane)
    ticket = TicketFactory()
    # this is the backlog column (so not displayed on the Kanban board)
    column = KanbanColumnFactory(
        title="Validate",
        order=0,
        lane=KanbanLaneFactory(title="Support", order=1),
    )
    KanbanCardFactory(ticket=ticket, column=column)
    assert lane is not ticket.kanban_card().column.lane
    url = reverse("crm.ticket.update", kwargs={"pk": ticket.pk})
    data = {
        "priority": PriorityFactory().pk,
        "title": "Orange Juice",
        "ticket_type": lane.pk,
    }
    response = client.post(url, data)
    assert 302 == response.status_code
    expect = reverse("ticket.detail", args=[ticket.pk])
    assert expect == response["Location"]
    ticket.refresh_from_db()
    card = KanbanCard.objects.get(ticket=ticket)
    # card = ticket.kanban_card()
    assert card is not None
    assert backlog == card.column
    assert lane == card.column.lane
