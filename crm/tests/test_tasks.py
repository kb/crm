# -*- encoding: utf-8 -*-
import pytest

from crm.tasks import rebuild_ticket_index, update_ticket_index
from crm.tests.factories import TicketFactory


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_rebuild_ticket_index():
    TicketFactory()
    TicketFactory()
    assert 2 == rebuild_ticket_index()


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_update_ticket_index():
    TicketFactory()
    ticket = TicketFactory()
    assert 1 == update_ticket_index(ticket.pk)
