# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse
from http import HTTPStatus

from contact.tests.factories import ContactFactory
from crm.tests.factories import NoteFactory, TicketFactory
from login.tests.factories import TEST_PASSWORD, UserFactory
from login.tests.fixture import perm_check


@pytest.mark.django_db
def test_note_create(perm_check):
    ticket = TicketFactory()
    url = reverse("crm.note.create", kwargs={"pk": ticket.pk})
    perm_check.staff(url)


@pytest.mark.django_db
def test_note_create_for_user(client):
    user = UserFactory(is_staff=False)
    contact = ContactFactory(user=user)
    ticket = TicketFactory(contact=contact)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("crm.note.create", kwargs={"pk": ticket.pk})
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_note_update(perm_check):
    note = NoteFactory()
    url = reverse("crm.note.update", kwargs={"pk": note.pk})
    perm_check.staff(url)


@pytest.mark.django_db
def test_note_update_for_user(client):
    user = UserFactory(is_staff=False)
    contact = ContactFactory(user=user)
    ticket = TicketFactory(contact=contact)
    note = NoteFactory(ticket=ticket)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("crm.note.update", kwargs={"pk": note.pk})
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_ticket_complete_for_user(client):
    user = UserFactory(is_staff=False)
    contact = ContactFactory(user=user)
    ticket = TicketFactory(contact=contact)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("crm.ticket.complete", kwargs={"pk": ticket.pk})
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_ticket_create(perm_check):
    contact = ContactFactory()
    url = reverse("crm.ticket.create", kwargs={"pk": contact.pk})
    perm_check.staff(url)


@pytest.mark.django_db
def test_ticket_create_for_user(client):
    user = UserFactory(is_staff=False)
    contact = ContactFactory(user=user)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(reverse("crm.ticket.create", args=[contact.pk]))
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_ticket_detail(perm_check):
    ticket = TicketFactory()
    url = reverse("ticket.detail", kwargs={"pk": ticket.pk})
    perm_check.staff(url)


@pytest.mark.django_db
def test_ticket_detail_user(client):
    """Check the contact user can view the ticket details."""
    user = UserFactory()
    contact = ContactFactory(user=user)
    ticket = TicketFactory(contact=contact)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("ticket.detail", kwargs={"pk": ticket.pk})
    response = client.get(url)
    assert 200 == response.status_code


@pytest.mark.django_db
def test_ticket_detail_user_not(client):
    """Make sure a different user cannot view the ticket details."""
    user = UserFactory()
    ContactFactory(user=user)
    ticket = TicketFactory()
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("ticket.detail", kwargs={"pk": ticket.pk})
    response = client.get(url)
    assert 403 == response.status_code


@pytest.mark.django_db
def test_ticket_update(perm_check):
    ticket = TicketFactory()
    url = reverse("crm.ticket.update", kwargs={"pk": ticket.pk})
    perm_check.staff(url)


@pytest.mark.django_db
def test_ticket_update_for_user(client):
    user = UserFactory(is_staff=False)
    contact = ContactFactory(user=user)
    ticket = TicketFactory(contact=contact)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(reverse("crm.ticket.update", args=[ticket.pk]))
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_ticket_update_funding(perm_check):
    ticket = TicketFactory()
    url = reverse("crm.ticket.update.funding", args=[ticket.pk])
    perm_check.staff(url)
