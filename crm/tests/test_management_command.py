# -*- encoding: utf-8 -*-
import pytest

from django.core.management import call_command

from crm.models import Funded, KanbanLane


@pytest.mark.django_db
def test_init_app():
    call_command("init-app-crm")
    x = Funded.objects.get(slug=Funded.FREE_OF_CHARGE)
    assert x.free_of_charge is True
    assert x.sales_order is False
    x = Funded.objects.get(slug=Funded.SALES_ORDER)
    assert x.free_of_charge is False
    assert x.sales_order is True
    x = KanbanLane.objects.get(title="Admin")
    assert x.funded is False
    x = KanbanLane.objects.get(title="Development")
    assert x.funded is True
    x = KanbanLane.objects.get(title="Sales")
    assert x.funded is False
    x = KanbanLane.objects.get(title="Support")
    assert x.funded is True


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_rebuild_ticket_index():
    call_command("rebuild_ticket_index")
