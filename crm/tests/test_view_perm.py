# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse
from http import HTTPStatus

from contact.tests.factories import ContactFactory
from crm.tests.factories import CrmContactFactory, NoteFactory, TicketFactory
from login.tests.factories import TEST_PASSWORD, UserFactory
from login.tests.fixture import perm_check
from login.tests.scenario import (
    default_scenario_login,
    get_user_staff,
    user_contractor,
)


@pytest.mark.django_db
def test_crm_contact_create(perm_check):
    contact = ContactFactory()
    url = reverse("crm.contact.create", kwargs={"pk": contact.pk})
    perm_check.staff(url)


@pytest.mark.django_db
def test_contact_project_children(perm_check):
    contact = ContactFactory()
    url = reverse("crm.contact.ticket.list", args=[contact.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_crm_contact_update(perm_check):
    contact = ContactFactory()
    crm_contact = CrmContactFactory(contact=contact)
    url = reverse("crm.contact.update", kwargs={"pk": crm_contact.pk})
    perm_check.staff(url)


@pytest.mark.django_db
def test_crm_settings_update(perm_check):
    url = reverse("crm.settings.update")
    perm_check.staff(url)


@pytest.mark.django_db
def test_note_create(perm_check):
    ticket = TicketFactory()
    url = reverse("crm.note.create", kwargs={"pk": ticket.pk})
    perm_check.staff(url)


@pytest.mark.django_db
def test_note_update(perm_check):
    note = NoteFactory()
    url = reverse("crm.note.update", kwargs={"pk": note.pk})
    perm_check.staff(url)


@pytest.mark.django_db
def test_ticket_complete(perm_check):
    ticket = TicketFactory()
    url = reverse("crm.ticket.complete", kwargs={"pk": ticket.pk})
    perm_check.staff(url)


@pytest.mark.django_db
def test_ticket_create(perm_check):
    contact = ContactFactory()
    url = reverse("crm.ticket.create", kwargs={"pk": contact.pk})
    perm_check.staff(url)


@pytest.mark.django_db
def test_ticket_detail(perm_check):
    ticket = TicketFactory()
    url = reverse("ticket.detail", kwargs={"pk": ticket.pk})
    perm_check.staff(url)


@pytest.mark.django_db
def test_ticket_detail_for_user(client):
    """Login as the client (user) who *owns* the ticket."""
    ticket = TicketFactory()
    assert (
        client.login(
            username=ticket.contact.user.username, password=TEST_PASSWORD
        )
        is True
    )
    response = client.get(reverse("ticket.detail", args=[ticket.pk]))
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_ticket_home(perm_check):
    TicketFactory()
    url = reverse("crm.ticket.home")
    perm_check.staff(url)


@pytest.mark.django_db
def test_ticket_update(perm_check):
    ticket = TicketFactory()
    url = reverse("crm.ticket.update", kwargs={"pk": ticket.pk})
    perm_check.staff(url)
