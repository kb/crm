# -*- encoding: utf-8 -*-
import pytest
import pytz

from datetime import datetime
from django.urls import reverse
from freezegun import freeze_time
from http import HTTPStatus

from api.tests.fixture import api_client
from contact.tests.factories import ContactFactory
from login.tests.factories import UserFactory

from .factories import PriorityFactory, TicketFactory


@pytest.mark.django_db
def test_ticket_list(api_client):
    user = UserFactory(
        email="web@pkimber.net",
        first_name="Patrick",
        last_name="Kimber",
        username="pkimber",
    )
    contact_1 = ContactFactory(user=UserFactory(username="patrick"))
    contact_2 = ContactFactory(user=UserFactory(username="andrea"))
    priority = PriorityFactory(name="high")
    with freeze_time(datetime(2020, 3, 11, 18, 30, 0, tzinfo=pytz.utc)):
        ticket_1 = TicketFactory(
            contact=contact_1,
            title="Grass Cutting",
            priority=priority,
            user=user,
        )
        ticket_2 = TicketFactory(
            contact=contact_2, priority=priority, title="Silage", user=user
        )
        ticket_3 = TicketFactory(
            contact=contact_1, priority=priority, title="Turnips", user=user
        )
    response = api_client.get(
        reverse("api:ticket-list"),
        content_type="application/vnd.api+json",
    )
    assert HTTPStatus.OK == response.status_code
    data = response.json()
    assert (
        data
        == {
            "data": [
                {
                    "type": "tickets",
                    "id": f"{ticket_1.id}",
                    "attributes": {
                        "complete": None,
                        "contact-name": "company_00",
                        "created": "2020-03-11T18:30:00Z",
                        "description": None,
                        "due": None,
                        "priority-name": "high",
                        "title": "Grass Cutting",
                        "user-assigned-name": "",
                    },
                    "relationships": {
                        "contact": {
                            "data": {
                                "type": "contact",
                                "id": f"{contact_1.id}",
                            }
                        },
                        "priority": {
                            "data": {
                                "type": "priorities",
                                "id": f"{priority.id}",
                            }
                        },
                        "user": {"data": {"type": "users", "id": f"{user.id}"}},
                        "user-assigned": {"data": None},
                    },
                },
                {
                    "type": "tickets",
                    "id": f"{ticket_2.id}",
                    "attributes": {
                        "complete": None,
                        "contact-name": "company_01",
                        "created": "2020-03-11T18:30:00Z",
                        "description": None,
                        "due": None,
                        "priority-name": "high",
                        "title": "Silage",
                        "user-assigned-name": "",
                    },
                    "relationships": {
                        "contact": {
                            "data": {
                                "type": "contact",
                                "id": f"{contact_2.id}",
                            }
                        },
                        "priority": {
                            "data": {
                                "type": "priorities",
                                "id": f"{priority.id}",
                            }
                        },
                        "user": {"data": {"type": "users", "id": f"{user.id}"}},
                        "user-assigned": {"data": None},
                    },
                },
                {
                    "type": "tickets",
                    "id": f"{ticket_3.id}",
                    "attributes": {
                        "complete": None,
                        "contact-name": "company_00",
                        "created": "2020-03-11T18:30:00Z",
                        "description": None,
                        "due": None,
                        "priority-name": "high",
                        "title": "Turnips",
                        "user-assigned-name": "",
                    },
                    "relationships": {
                        "contact": {
                            "data": {
                                "type": "contact",
                                "id": f"{contact_1.id}",
                            }
                        },
                        "priority": {
                            "data": {
                                "type": "priorities",
                                "id": f"{priority.id}",
                            }
                        },
                        "user": {"data": {"type": "users", "id": f"{user.id}"}},
                        "user-assigned": {"data": None},
                    },
                },
            ]
        }
        == data
    )
