# -*- encoding: utf-8 -*-
import csv
import pytest
import pytz

from datetime import date, datetime
from freezegun import freeze_time

from contact.tests.factories import ContactFactory
from crm.tests.factories import TicketFactory
from crm.reports import ContactTicketCountReport
from crm.tests.factories import CrmContactFactory, IndustryFactory
from login.tests.factories import UserFactory
from report.models import ReportSchedule, ReportSpecification


@pytest.mark.django_db
def test_contact_ticket_count_report():
    user = UserFactory()
    # test data
    with freeze_time(datetime(2025, 3, 4, 1, 1, 1, tzinfo=pytz.utc)):
        user_1 = UserFactory(
            first_name="Alan", last_name="Jones", email="alan@email.com"
        )
        contact_1 = ContactFactory(user=user_1, company_name="")
        industry = IndustryFactory(name="Farming")
        CrmContactFactory(contact=contact_1, industry=industry)
        TicketFactory(contact=contact_1)
        # contact without a 'CrmContact' record
        user_2 = UserFactory(
            first_name="P", last_name="Kimber", email="patrick@email.com"
        )
        contact_2 = ContactFactory(user=user_2, company_name="")
    # initilise the report
    ContactTicketCountReport().init_report()
    parameters = {"from_date": date(2025, 3, 1), "to_date": date(2025, 3, 30)}
    report_schedule = ReportSpecification.objects.schedule(
        ContactTicketCountReport.REPORT_SLUG,
        user,
        parameters=parameters,
    )
    schedule_pks = ReportSchedule.objects.process()
    assert 1 == len(schedule_pks)
    schedule_pk = schedule_pks[0]
    report_schedule = ReportSchedule.objects.get(pk=schedule_pk)
    reader = csv.reader(open(report_schedule.output_file.path), "excel")
    first_row = None
    result = []
    for row in reader:
        if not first_row:
            first_row = row
        else:
            result.append(row)
    assert [
        "id",
        "name",
        "industry",
        "email",
        "website",
        "phone",
        "created",
        "url",
        "ticket_count",
    ] == first_row
    assert [
        [
            f"{contact_1.pk}",
            "Alan Jones",
            "Farming",
            "alan@email.com",
            "",
            "",
            "04/03/2025",
            f"http://localhost:8000/contact/{contact_1.pk}/",
            "1",
        ],
        [
            f"{contact_2.pk}",
            "P Kimber",
            "",
            "patrick@email.com",
            "",
            "",
            "04/03/2025",
            f"http://localhost:8000/contact/{contact_2.pk}/",
            "0",
        ],
    ] == result
