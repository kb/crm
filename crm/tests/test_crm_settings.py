# -*- encoding: utf-8 -*-
import pytest

from .factories import CrmSettingsFactory, KanbanLaneFactory


@pytest.mark.django_db
def test_str():
    assert "CRM Settings: Kanban lane for project work: Development" == str(
        CrmSettingsFactory(project_lane=KanbanLaneFactory(title="Development"))
    )
