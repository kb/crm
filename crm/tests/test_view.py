# -*- encoding: utf-8 -*-
import pytest

from datetime import date
from django.core.management import call_command
from django.urls import reverse
from http import HTTPStatus

from base.url_utils import url_with_querystring
from chat.models import Channel, Message
from chat.tests.factories import ChannelFactory
from contact.tests.factories import ContactFactory
from crm.models import (
    CrmContact,
    CrmSettings,
    Funded,
    KanbanCard,
    Ticket,
    UserTicketFavourite,
)
from crm.tests.factories import (
    CrmContactFactory,
    CrmSettingsFactory,
    FundedFactory,
    IndustryFactory,
    KanbanCardFactory,
    KanbanColumnFactory,
    KanbanLaneFactory,
    NoteFactory,
    PriorityFactory,
    TicketFactory,
    UserSettingsFactory,
)
from invoice.models import TimeRecord
from invoice.tests.factories import QuickTimeRecordFactory, TimeRecordFactory
from login.tests.factories import TEST_PASSWORD, UserFactory


@pytest.mark.django_db
@pytest.mark.parametrize(
    "ticket_status,ticket_type,expect",
    [
        (Ticket.STATUS_ALL, Ticket.TYPE_ALL, ["a", "b", "c", "d", "f"]),
        (Ticket.STATUS_ALL, Ticket.TYPE_DEFAULT, ["a", "c", "d", "f"]),
        ("", Ticket.TYPE_PROJECT_CHILDREN, ["b"]),
        (Ticket.STATUS_CURRENT, Ticket.TYPE_ALL, ["a", "b", "d", "f"]),
    ],
)
@pytest.mark.django_db
def test_contact_ticket_list(client, ticket_status, ticket_type, expect):
    user = UserFactory(is_staff=True)
    """A list of tickets and top level MPTT parent tickets."""
    sales_lane = KanbanLaneFactory(title="Sales")
    KanbanColumnFactory(lane=sales_lane, order=0, title="Backlog")
    development_lane = KanbanLaneFactory(title="Development")
    KanbanColumnFactory(lane=development_lane, order=0, title="Backlog")
    support_lane = KanbanLaneFactory(title="Support")
    KanbanColumnFactory(lane=support_lane, order=0, title="Backlog")
    contact = ContactFactory()
    # settings
    CrmSettingsFactory(project_lane=development_lane)
    # development ticket (and parent)
    t1 = TicketFactory(title="a", contact=contact)
    KanbanCard.objects.init_kanban_card(t1, development_lane, user)
    # development ticket (and child)
    t2 = TicketFactory(title="b", contact=contact, parent=t1)
    KanbanCard.objects.init_kanban_card(t2, development_lane, user)
    # support ticket
    t3 = TicketFactory(title="c", contact=contact)
    KanbanCard.objects.init_kanban_card(t3, support_lane, user)
    t3.set_complete(user)
    t3.save()
    # development ticket
    t4 = TicketFactory(title="d", contact=contact)
    KanbanCard.objects.init_kanban_card(t4, development_lane, user)
    # another contact
    t5 = TicketFactory(title="e", contact=ContactFactory())
    KanbanCard.objects.init_kanban_card(t5, support_lane, user)
    # sales ticket
    t6 = TicketFactory(title="f", contact=contact)
    KanbanCard.objects.init_kanban_card(t6, sales_lane, user)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = url_with_querystring(
        reverse("crm.contact.ticket.list", args=[contact.pk]),
        ticket_status=ticket_status,
        ticket_type=ticket_type,
    )
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    assert "ticket_list" in response.context
    qs = response.context["ticket_list"]
    assert expect == sorted([x.title for x in qs])


@pytest.mark.django_db
def test_crm_contact_create(client):
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    contact = ContactFactory()
    url = reverse("crm.contact.create", kwargs={"pk": contact.pk})
    data = {"industry": IndustryFactory(name="Agriculture").pk}
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code
    expect = reverse("contact.detail", args=[contact.pk])
    assert expect == response.url
    crm_contact = CrmContact.objects.get(contact=contact)
    assert "Agriculture" == crm_contact.industry.name


@pytest.mark.django_db
def test_crm_contact_update(client):
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    contact = ContactFactory()
    crm_contact = CrmContactFactory(contact=contact)
    url = reverse("crm.contact.update", kwargs={"pk": crm_contact.pk})
    data = {"industry": IndustryFactory(name="Agriculture").pk}
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code
    expect = reverse("contact.detail", args=[contact.pk])
    assert expect == response.url
    crm_contact = CrmContact.objects.get(contact=contact)
    assert "Agriculture" == crm_contact.industry.name


@pytest.mark.django_db
def test_crm_settings_update(client):
    # login
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("crm.settings.update")
    kanban_lane = KanbanLaneFactory()
    response = client.post(url, data={"project_lane": kanban_lane.pk})
    # check
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert reverse("project.settings") == response.url
    assert 1 == CrmSettings.objects.count()
    crm_settings = CrmSettings.load()
    assert kanban_lane == crm_settings.project_lane


@pytest.mark.django_db
def test_ticket_children(client):
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    t1 = TicketFactory(title="t1")
    t2 = TicketFactory(title="t2", parent=t1)
    TicketFactory(title="t3", parent=t2)
    url = reverse("crm.ticket.children", kwargs={"pk": t1.pk})
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    assert "nodes" in response.context
    nodes = response.context["nodes"]
    assert ["t1", "t2", "t3"] == [x.title for x in nodes]


@pytest.mark.django_db
def test_ticket_create_child(client):
    ChannelFactory(slug=Channel.ACTIVITY)
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    contact = ContactFactory()
    parent_ticket = TicketFactory(contact=contact)
    lane = KanbanLaneFactory(title="Development")
    KanbanColumnFactory(lane=lane, order=0, title="Backlog")
    KanbanColumnFactory(lane=lane, order=1, title="Specify")
    assert parent_ticket.children.count() == 0
    priority = PriorityFactory()
    url = reverse("crm.ticket.create.child", kwargs={"pk": parent_ticket.pk})
    data = {
        "priority": priority.pk,
        "title": "Apple Juice",
        "ticket_type": lane.pk,
    }
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    ticket = Ticket.objects.get(title="Apple Juice")
    expect = "{}?highlight={}".format(
        reverse("crm.ticket.children", args=[parent_ticket.pk]), ticket.pk
    )
    assert expect == response.url
    assert ticket.contact == contact
    assert ticket.parent == parent_ticket
    card = ticket.kanban_card()
    assert card is not None
    assert KanbanCard.ACTIVE == card.state
    assert lane == card.column.lane
    assert "Apple Juice" == ticket.title
    assert parent_ticket.children.count() == 1
    assert parent_ticket.children.first() == ticket


@pytest.mark.django_db
def test_ticket_create_child_empty_ticket_type(client):
    """Create a child ticket with an empty ticket type."""
    ChannelFactory(slug=Channel.ACTIVITY)
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    contact = ContactFactory()
    parent_ticket = TicketFactory(contact=contact)
    assert parent_ticket.children.count() == 0
    priority = PriorityFactory()
    url = reverse("crm.ticket.create.child", kwargs={"pk": parent_ticket.pk})
    data = {"priority": priority.pk, "title": "Apple Juice"}
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code
    ticket = Ticket.objects.get(title="Apple Juice")
    expect = "{}?highlight={}".format(
        reverse("crm.ticket.children", args=[parent_ticket.pk]), ticket.pk
    )
    assert expect == response.url
    assert parent_ticket.children.count() == 1
    assert parent_ticket.children.first() == ticket
    assert ticket.kanban_card() is None


@pytest.mark.django_db
def test_ticket_detail(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    ticket = TicketFactory()
    NoteFactory(ticket=ticket, title="a", user=UserFactory(username="a"))
    TimeRecordFactory(ticket=ticket, title="b", user=UserFactory(username="b"))
    NoteFactory(ticket=ticket, title="c", user=UserFactory(username="c"))
    response = client.get(reverse("ticket.detail", kwargs={"pk": ticket.pk}))
    assert HTTPStatus.OK == response.status_code
    assert "object_list" in response.context, response.context.keys()
    ticket_activity = response.context["object_list"]
    assert ["c", "b", "a"] == [x["user__username"] for x in ticket_activity]


@pytest.mark.django_db
def test_ticket_complete(client):
    ChannelFactory(slug=Channel.ACTIVITY)
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    ticket = TicketFactory()
    assert bool(ticket.complete) is False
    # test
    url = reverse("crm.ticket.complete", kwargs={"pk": ticket.pk})
    response = client.post(url)
    # check
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    expect = reverse("ticket.detail", args=[ticket.pk])
    assert expect == response.url
    ticket.refresh_from_db()
    assert bool(ticket.complete) is True
    assert user == ticket.complete_user


@pytest.mark.django_db
def test_ticket_complete_on_kanban_board_not(client):
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    ticket = TicketFactory()
    lane = KanbanLaneFactory(order=1)
    column = KanbanColumnFactory(lane=lane, order=1)
    KanbanColumnFactory(lane=lane, order=2)
    KanbanCardFactory(ticket=ticket, column=column, state=KanbanCard.ACTIVE)
    # test
    url = reverse("crm.ticket.complete", kwargs={"pk": ticket.pk})
    response = client.post(url)
    # check
    assert HTTPStatus.OK == response.status_code
    assert {
        "__all__": ["The ticket cannot be completed.  Is it on a Kanban board?"]
    } == response.context["form"].errors
    ticket.refresh_from_db()
    assert bool(ticket.complete) is False


@pytest.mark.django_db
def test_ticket_complete_toggle(client):
    """Completed ticket is un-complete."""
    ChannelFactory(slug=Channel.ACTIVITY)
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    ticket = TicketFactory()
    ticket.set_complete(user)
    ticket.save()
    ticket.refresh_from_db()
    assert bool(ticket.complete) is True
    assert user == ticket.complete_user
    # test
    url = reverse("crm.ticket.complete", kwargs={"pk": ticket.pk})
    response = client.post(url)
    # check
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    expect = reverse("ticket.detail", args=[ticket.pk])
    assert expect == response.url
    ticket.refresh_from_db()
    assert bool(ticket.complete) is False
    assert ticket.complete_user is None


@pytest.mark.django_db
def test_ticket_create(client):
    ChannelFactory(slug=Channel.ACTIVITY)
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    contact = ContactFactory()
    contact = ContactFactory(
        company_name="S 1234567 T 1234567 U 1234567 V 1234567 W 1234567 "
    )
    # funded = Funded.objects.get(slug=Funded.SALES_ORDER)
    lane = KanbanLaneFactory(title="Development")
    KanbanColumnFactory(lane=lane, order=0, title="Backlog")
    KanbanColumnFactory(lane=lane, order=1, title="Specify")
    priority = PriorityFactory()
    assert 0 == Message.objects.count()
    url = reverse("crm.ticket.create", kwargs={"pk": contact.pk})
    data = {
        # "funded": funded.pk,
        "priority": priority.pk,
        "ticket_type": lane.pk,
        "title": (
            "A 1234567 B 1234567 C 1234567 D 1234567 E 1234567 "
            "F 1234567 G 1234567 H 1234567 I 1234567 J 1234567 "
        ),
    }
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    ticket = Ticket.objects.get(contact=contact)
    expect = reverse("ticket.detail", args=[ticket.pk])
    # assert Funded.SALES_ORDER == ticket.funded.slug
    assert expect == response.url
    assert priority.pk == ticket.priority.pk
    assert ticket.fixed_price is False
    # assert ticket.funded is None
    assert (
        "A 1234567 B 1234567 C 1234567 D 1234567 E 1234567 "
        "F 1234567 G 1234567 H 1234567 I 1234567 J 1234567"
    ) == ticket.title
    card = ticket.kanban_card()
    assert card is not None
    assert lane == card.column.lane
    assert 1 == Message.objects.count()
    message = Message.objects.first()
    assert (
        "[Ticket #{}](http://localhost:8000/crm/ticket/{}/), "
        "S 1234567 T 1234567 U 1234 ... "
        "A 1234567 B 1234567 C 1234567 D 1234 ... "
        "Created (by *staff*)".format(ticket.pk, ticket.pk)
    ) == message.title


@pytest.mark.django_db
def test_ticket_create_no_ticket_type(client):
    ChannelFactory(slug=Channel.ACTIVITY)
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    contact = ContactFactory()
    priority = PriorityFactory()
    url = reverse("crm.ticket.create", kwargs={"pk": contact.pk})
    data = {
        "fixed_price": False,
        "priority": priority.pk,
        "title": "Apple Juice",
    }
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    ticket = Ticket.objects.get(contact=contact)
    assert ticket.fixed_price is False
    assert priority.pk == ticket.priority.pk
    assert "Apple Juice" == ticket.title


@pytest.mark.django_db
def test_ticket_favourite(client):
    user = UserFactory(is_staff=True)
    # login
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    ticket = TicketFactory()
    url = reverse("crm.ticket.favourite", args=[ticket.pk])
    response = client.post(url)
    assert HTTPStatus.FOUND == response.status_code
    expect = reverse("ticket.detail", args=[ticket.pk])
    assert expect == response.url
    favourite = UserTicketFavourite.objects.get(user=user, ticket=ticket)
    assert favourite.is_deleted is False


@pytest.mark.django_db
def test_ticket_quick_time_start(client):
    user = UserFactory(username="staff", is_staff=True)
    quick = QuickTimeRecordFactory(user=user)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    ticket = TicketFactory()
    url = reverse(
        "crm.ticket.quick.time.start",
        kwargs={"pk": ticket.pk, "quick_pk": quick.pk},
    )
    assert 0 == TimeRecord.objects.count()
    response = client.post(url)
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("ticket.detail", args=[ticket.pk]) == response.url
    assert 1 == TimeRecord.objects.count()
    time_record = TimeRecord.objects.first()
    assert quick.time_code.description == time_record.time_code.description


@pytest.mark.django_db
def test_ticket_unfavourite(client):
    user = UserFactory(is_staff=True)
    # login
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    ticket = TicketFactory()
    favourite = UserTicketFavourite.objects.create_user_ticket_favourite(
        user=user, ticket=ticket
    )
    assert favourite.is_deleted is False
    url = reverse("crm.ticket.unfavourite", args=[ticket.pk])
    response = client.post(url)
    assert HTTPStatus.FOUND == response.status_code
    expect = reverse("ticket.detail", args=[ticket.pk])
    assert expect == response.url
    favourite.refresh_from_db()
    assert favourite.is_deleted is True


@pytest.mark.django_db
def test_ticket_move(client):
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    ticket_from = TicketFactory()
    ticket_to = TicketFactory()
    data = {"to_ticket": ticket_to.pk}
    url = reverse("crm.ticket.move", kwargs={"pk": ticket_from.pk})
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    # e.g. /crm/ticket/161/children/?highlight=162
    expect = reverse(
        "crm.ticket.move.confirm", args=[ticket_from.pk, ticket_to.pk]
    )
    assert expect == response.url


@pytest.mark.django_db
def test_ticket_move_confirm(client):
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    ticket_from = TicketFactory()
    ticket_to = TicketFactory()
    url = reverse(
        "crm.ticket.move.confirm",
        kwargs={"pk": ticket_from.pk, "to_pk": ticket_to.pk},
    )
    response = client.post(url)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    # e.g. '/crm/ticket/226/children/?highlight=225'
    expect = "{}?highlight={}".format(
        reverse("crm.ticket.children", args=[ticket_to.pk]), ticket_from.pk
    )
    assert expect == response.url
    ticket_from.refresh_from_db()
    assert ticket_from.parent == ticket_to


@pytest.mark.django_db
def test_ticket_move_invalid_ticket_number(client):
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    ticket_from = TicketFactory()
    ticket_to = TicketFactory()
    invalid_ticket_number = ticket_from.pk + ticket_to.pk
    data = {"to_ticket": invalid_ticket_number}
    url = reverse("crm.ticket.move", kwargs={"pk": ticket_from.pk})
    response = client.post(url, data)
    assert HTTPStatus.OK == response.status_code
    assert "form" in response.context
    assert {
        "to_ticket": [
            "Ticket '{}' does not exist".format(invalid_ticket_number)
        ]
    } == response.context["form"].errors


@pytest.mark.django_db
def test_ticket_parent_clear_confirm(client):
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    ticket = TicketFactory(parent=TicketFactory())
    assert ticket.parent is not None
    url = reverse("crm.ticket.parent.clear.confirm", kwargs={"pk": ticket.pk})
    response = client.post(url)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert reverse("ticket.detail", args=[ticket.pk]) == response.url
    ticket.refresh_from_db()
    assert ticket.parent is None


@pytest.mark.django_db
def test_ticket_update(client):
    user = UserFactory(username="staff", is_staff=True)
    UserSettingsFactory(user=user, display_notify_list=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    # funded = Funded.objects.get(slug=Funded.SALES_ORDER)
    priority = PriorityFactory()
    ticket = TicketFactory()
    assert ticket.fixed_price is False
    assert 0 == Message.objects.count()
    url = reverse("crm.ticket.update", kwargs={"pk": ticket.pk})
    data = {
        "due": date(3000, 1, 31),
        "due_notify": date(3000, 1, 1),
        "fixed_price": True,
        "priority": priority.pk,
        "title": "Apple Juice",
    }
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    expect = reverse("ticket.detail", args=[ticket.pk])
    assert expect == response.url
    ticket.refresh_from_db()
    assert ticket.due == date(3000, 1, 31)
    assert ticket.due_notify == date(3000, 1, 1)
    assert ticket.fixed_price is False
    assert priority.pk == ticket.priority.pk
    assert "Apple Juice" == ticket.title
    assert 0 == Message.objects.count()


@pytest.mark.django_db
def test_ticket_update_change_user_assigned(client):
    ChannelFactory(slug=Channel.ACTIVITY)
    user = UserFactory(is_staff=True)
    new_user = UserFactory(is_staff=True, username="fred.smith")
    UserSettingsFactory(user=user, display_notify_list=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    priority = PriorityFactory()
    ticket = TicketFactory(user_assigned=user)
    assert ticket.fixed_price is False
    assert 0 == Message.objects.count()
    url = reverse("crm.ticket.update", kwargs={"pk": ticket.pk})
    data = {
        "due": date(3000, 1, 31),
        "due_notify": date(3000, 1, 1),
        # "funded": funded.pk,
        "priority": priority.pk,
        "title": "Apple Juice",
        "user_assigned": new_user.pk,
    }
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    expect = reverse("ticket.detail", args=[ticket.pk])
    assert expect == response.url
    ticket.refresh_from_db()
    assert ticket.due == date(3000, 1, 31)
    assert ticket.due_notify == date(3000, 1, 1)
    assert ticket.funded is None
    # assert Funded.SALES_ORDER == ticket.funded.slug
    assert priority.pk == ticket.priority.pk
    assert "Apple Juice" == ticket.title
    assert 1 == Message.objects.count()
    message = Message.objects.first()
    assert "Assigned to fred.smith :construction_worker_woman:" in message.title


@pytest.mark.django_db
def test_ticket_update_change_user_assigned_empty(client):
    ChannelFactory(slug=Channel.ACTIVITY)
    user = UserFactory(is_staff=True)
    UserSettingsFactory(user=user, display_notify_list=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    priority = PriorityFactory()
    ticket = TicketFactory(user_assigned=user)
    assert 0 == Message.objects.count()
    url = reverse("crm.ticket.update", kwargs={"pk": ticket.pk})
    data = {
        "due": date(3000, 1, 31),
        "due_notify": date(3000, 1, 1),
        "fixed_price": True,
        "priority": priority.pk,
        "title": "Apple Juice",
        "user_assigned": "",
    }
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    expect = reverse("ticket.detail", args=[ticket.pk])
    assert expect == response.url
    assert 1 == Message.objects.count()
    message = Message.objects.first()
    assert "Assigned to nobody!" in message.title


@pytest.mark.django_db
def test_ticket_update_due_empty_with_notify(client):
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    priority = PriorityFactory()
    ticket = TicketFactory(due_notify=date.today())
    assert ticket.fixed_price is False
    url = reverse("crm.ticket.update", kwargs={"pk": ticket.pk})
    data = {
        "due_notify": date(3000, 1, 31),
        "fixed_price": True,
        "priority": priority.pk,
        "title": "Apple Juice",
    }
    response = client.post(url, data)
    assert HTTPStatus.OK == response.status_code
    assert {
        "__all__": ["You have a notification date, please add a due date"]
    } == response.context["form"].errors


@pytest.mark.django_db
def test_ticket_update_due_notify_empty(client):
    """The user can enter a due date, but not a notify date."""
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    priority = PriorityFactory()
    ticket = TicketFactory()
    assert ticket.fixed_price is False
    url = reverse("crm.ticket.update", kwargs={"pk": ticket.pk})
    data = {
        "due": date(3000, 1, 31),
        "fixed_price": True,
        "priority": priority.pk,
        "title": "Apple Juice",
    }
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    expect = reverse("ticket.detail", args=[ticket.pk])
    assert expect == response.url


@pytest.mark.django_db
def test_ticket_update_due_notify_hide(client):
    """Hide the notify field if the user isn't displaying the notify table."""
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    PriorityFactory()
    ticket = TicketFactory()
    assert ticket.fixed_price is False
    url = reverse("crm.ticket.update", kwargs={"pk": ticket.pk})
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    form = response.context["form"]
    assert "due" in form.fields, list(form.fields.keys())
    assert "due_notify" not in form.fields, list(form.fields.keys())


@pytest.mark.django_db
def test_ticket_update_due_notify_show(client):
    """Show the notify field if the ticket has a notify date."""
    user = UserFactory(username="staff", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    PriorityFactory()
    ticket = TicketFactory(due=date.today(), due_notify=date.today())
    assert ticket.fixed_price is False
    url = reverse("crm.ticket.update", kwargs={"pk": ticket.pk})
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    form = response.context["form"]
    assert "due" in form.fields, list(form.fields.keys())
    assert "due_notify" in form.fields, list(form.fields.keys())


@pytest.mark.django_db
def test_ticket_update_due_notify_too_late(client):
    user = UserFactory(username="staff", is_staff=True)
    UserSettingsFactory(user=user, display_notify_list=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    priority = PriorityFactory()
    ticket = TicketFactory()
    assert ticket.fixed_price is False
    url = reverse("crm.ticket.update", kwargs={"pk": ticket.pk})
    data = {
        "due": date(3000, 1, 31),
        "due_notify": date(3000, 2, 1),
        "fixed_price": True,
        "priority": priority.pk,
        "title": "Apple Juice",
    }
    response = client.post(url, data)
    assert HTTPStatus.OK == response.status_code
    assert {
        "__all__": [
            (
                "The notification date for the due date must "
                "be before the due date."
            )
        ]
    } == response.context["form"].errors


@pytest.mark.django_db
def test_ticket_update_funding(client):
    ChannelFactory(slug=Channel.ACTIVITY)
    user = UserFactory(username="patrick", is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    funded = FundedFactory(slug=Funded.SALES_ORDER, name="Sales Order")
    ticket = TicketFactory(
        contact=ContactFactory(company_name="KB"), title="Apple", funded=None
    )
    assert 0 == Message.objects.count()
    url = reverse("crm.ticket.update.funding", args=[ticket.pk])
    data = {"funded": funded.pk}
    response = client.post(url, data)
    assert HTTPStatus.FOUND == response.status_code
    expect = reverse("ticket.detail", args=[ticket.pk])
    assert expect == response.url
    ticket.refresh_from_db()
    assert funded == ticket.funded
    assert 1 == Message.objects.count()
    message = Message.objects.first()
    assert (
        "[Ticket #{}](http://localhost:8000/crm/ticket/{}/), "
        "KB, Apple, :moneybag: Funded by Sales Order "
        "(by *patrick*)".format(ticket.pk, ticket.pk)
    ) == message.title


@pytest.mark.django_db
def test_ticket_update_funding_required(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    funded = FundedFactory(slug=Funded.SALES_ORDER)
    ticket = TicketFactory(funded=None)
    url = reverse("crm.ticket.update.funding", args=[ticket.pk])
    response = client.post(url, {})
    assert HTTPStatus.OK == response.status_code
    assert {"funded": ["This field is required."]} == response.context[
        "form"
    ].errors
