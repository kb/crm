# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse

from contact.tests.factories import ContactFactory
from crm.tests.factories import (
    CrmContactFactory,
    KanbanCardFactory,
    KanbanColumnFactory,
    KanbanLaneFactory,
    TicketFactory,
)
from login.tests.factories import UserFactory
from login.tests.fixture import perm_check


@pytest.mark.django_db
def test_contact_update(perm_check):
    contact = ContactFactory()
    crm_contact = CrmContactFactory(contact=contact)
    url = reverse("crm.contact.update", args=[crm_contact.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_kanban(perm_check):
    lane = KanbanLaneFactory()
    url = reverse("crm.kanban.lane.board", args=[lane.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_kanban_lane_backlog(perm_check):
    lane = KanbanLaneFactory()
    backlog_column = KanbanColumnFactory(lane=lane, order=0, title="Backlog")
    # pending backlog (order == 0)
    KanbanCardFactory(ticket=TicketFactory(), column=backlog_column, order=0)
    # backlog
    KanbanCardFactory(ticket=TicketFactory(), column=backlog_column, order=1)
    url = reverse("crm.kanban.lane.backlog", args=[lane.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_project_ticket_due_list(perm_check):
    url = reverse("crm.project.ticket.due.list")
    perm_check.staff(url)


@pytest.mark.django_db
def test_project_ticket_priority_list(perm_check):
    url = reverse("crm.project.ticket.priority.list")
    perm_check.staff(url)


@pytest.mark.django_db
def test_ticket_children(perm_check):
    ticket = TicketFactory()
    url = reverse("crm.ticket.children", kwargs={"pk": ticket.pk})
    perm_check.staff(url)


@pytest.mark.django_db
def test_ticket_create_child(perm_check):
    ticket = TicketFactory()
    url = reverse("crm.ticket.create.child", kwargs={"pk": ticket.pk})
    perm_check.staff(url)


@pytest.mark.django_db
def test_ticket_complete(perm_check):
    ticket = TicketFactory()
    url = reverse("crm.ticket.complete", kwargs={"pk": ticket.pk})
    perm_check.staff(url)


@pytest.mark.django_db
def test_ticket_favourite(perm_check):
    ticket = TicketFactory()
    url = reverse("crm.ticket.favourite", kwargs={"pk": ticket.pk})
    perm_check.staff(url)


@pytest.mark.django_db
def test_ticket_kanban_card_update(perm_check):
    ticket = TicketFactory()
    KanbanCardFactory(ticket=ticket)
    url = reverse("crm.ticket.kanban.card.update", kwargs={"pk": ticket.pk})
    perm_check.staff(url)


# @pytest.mark.django_db
# def test_ticket_kanban_update(perm_check):
#     card = KanbanCardFactory()
#     url = reverse('crm.ticket.kanban.update', kwargs={'pk': card.pk})
#     perm_check.staff(url)


@pytest.mark.django_db
def test_ticket_list(perm_check):
    url = reverse("crm.ticket.list")
    perm_check.staff(url)


@pytest.mark.django_db
def test_ticket_move(perm_check):
    ticket_from = TicketFactory()
    url = reverse("crm.ticket.move", kwargs={"pk": ticket_from.pk})
    perm_check.staff(url)


@pytest.mark.django_db
def test_ticket_move_confirm(perm_check):
    ticket_from = TicketFactory()
    ticket_to = TicketFactory()
    url = reverse(
        "crm.ticket.move.confirm",
        kwargs={"pk": ticket_from.pk, "to_pk": ticket_to.pk},
    )
    perm_check.staff(url)


@pytest.mark.django_db
def test_ticket_parent_clear_confirm(perm_check):
    ticket = TicketFactory()
    url = reverse("crm.ticket.parent.clear.confirm", kwargs={"pk": ticket.pk})
    perm_check.staff(url)


@pytest.mark.django_db
def test_ticket_unfavourite(perm_check):
    ticket = TicketFactory()
    url = reverse("crm.ticket.unfavourite", kwargs={"pk": ticket.pk})
    perm_check.staff(url)


@pytest.mark.django_db
def test_user_settings(perm_check):
    UserFactory()
    perm_check.staff(reverse("crm.user.settings.update"))
