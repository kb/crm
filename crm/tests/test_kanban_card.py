# -*- encoding: utf-8 -*-
import pytest

from django.utils import timezone

from crm.models import CrmError, KanbanAudit, KanbanCard
from crm.tests.factories import (
    KanbanCardFactory,
    KanbanColumnFactory,
    KanbanLaneFactory,
    TicketFactory,
)
from login.tests.factories import UserFactory


@pytest.mark.django_db
def test_add_to_backlog():
    user = UserFactory()
    lane = KanbanLaneFactory()
    backlog_column = KanbanColumnFactory(lane=lane, order=0, title="Backlog")
    c1 = KanbanCardFactory(column=backlog_column, order=1)
    card = KanbanCardFactory(column=backlog_column, order=0)
    card.add_to_backlog(user)
    card.refresh_from_db()
    audit = KanbanAudit.objects.get(card=card)
    assert audit.column == backlog_column
    assert audit.created.date() == timezone.now().date()
    assert audit.order == 1
    assert audit.state == KanbanCard.ACTIVE
    assert audit.user == user
    c1.refresh_from_db()
    assert 2 == c1.order
    audit = KanbanAudit.objects.get(card=c1)
    assert audit.column == backlog_column
    assert audit.created.date() == timezone.now().date()
    assert audit.order == 2
    assert audit.state == KanbanCard.ACTIVE
    assert audit.user == user


@pytest.mark.django_db
def test_add_to_backlog_not_backlog():
    user = UserFactory()
    lane = KanbanLaneFactory()
    backlog_column = KanbanColumnFactory(lane=lane, order=1, title="Specify")
    card = KanbanCardFactory(column=backlog_column, order=0)
    with pytest.raises(CrmError) as e:
        card.add_to_backlog(user)
    assert "The 'Specify' column is not a backlog column" in str(e.value)


@pytest.mark.django_db
def test_add_to_backlog_not_pending():
    user = UserFactory()
    lane = KanbanLaneFactory()
    backlog_column = KanbanColumnFactory(lane=lane, order=0, title="Backlog")
    card = KanbanCardFactory(column=backlog_column, order=1)
    with pytest.raises(CrmError) as e:
        card.add_to_backlog(user)
    assert "It is not in the pending state" in str(e.value)


@pytest.mark.django_db
def test_backlog_move_down():
    user = UserFactory()
    lane = KanbanLaneFactory()
    backlog_column = KanbanColumnFactory(lane=lane, order=0, title="Backlog")
    c1 = KanbanCardFactory(column=backlog_column, order=8)
    c2 = KanbanCardFactory(column=backlog_column, order=9)
    c1.backlog_move_down(user)
    c1.refresh_from_db()
    assert 9 == c1.order
    audit = KanbanAudit.objects.get(card=c1)
    assert audit.column == backlog_column
    assert audit.created.date() == timezone.now().date()
    assert audit.order == 9
    assert audit.state == KanbanCard.ACTIVE
    assert audit.user == user
    audit = KanbanAudit.objects.get(card=c2)
    assert audit.column == backlog_column
    assert audit.created.date() == timezone.now().date()
    assert audit.order == 8
    assert audit.state == KanbanCard.ACTIVE
    assert audit.user == user


@pytest.mark.django_db
def test_backlog_move_down_insert():
    user = UserFactory()
    lane = KanbanLaneFactory()
    backlog_column = KanbanColumnFactory(lane=lane, order=0, title="Backlog")
    c1 = KanbanCardFactory(column=backlog_column, order=1)
    c2 = KanbanCardFactory(column=backlog_column, order=2)
    c3 = KanbanCardFactory(column=backlog_column, order=3)
    c4 = KanbanCardFactory(column=backlog_column, order=4)
    c2.backlog_move_down(user)
    for card in (c1, c2, c3, c4):
        card.refresh_from_db()
    assert c1.order == 1
    assert c3.order == 2
    assert c2.order == 3
    assert c4.order == 4


@pytest.mark.django_db
def test_backlog_move_up():
    user = UserFactory()
    lane = KanbanLaneFactory()
    backlog_column = KanbanColumnFactory(lane=lane, order=0, title="Backlog")
    c1 = KanbanCardFactory(column=backlog_column, order=7)
    c2 = KanbanCardFactory(column=backlog_column, order=8)
    c2.backlog_move_up(user)
    c2.refresh_from_db()
    assert 7 == c2.order
    audit = KanbanAudit.objects.get(card=c2)
    assert audit.column == backlog_column
    assert audit.created.date() == timezone.now().date()
    assert audit.order == 7
    assert audit.state == KanbanCard.ACTIVE
    assert audit.user == user
    audit = KanbanAudit.objects.get(card=c1)
    assert audit.column == backlog_column
    assert audit.created.date() == timezone.now().date()
    assert audit.order == 8
    assert audit.state == KanbanCard.ACTIVE
    assert audit.user == user


@pytest.mark.django_db
def test_backlog_move_up_insert():
    user = UserFactory()
    lane = KanbanLaneFactory()
    backlog_column = KanbanColumnFactory(lane=lane, order=0, title="Backlog")
    c1 = KanbanCardFactory(column=backlog_column, order=1)
    c2 = KanbanCardFactory(column=backlog_column, order=2)
    c3 = KanbanCardFactory(column=backlog_column, order=3)
    c4 = KanbanCardFactory(column=backlog_column, order=4)
    c3.backlog_move_up(user)
    for card in (c1, c2, c3, c4):
        card.refresh_from_db()
    assert c1.order == 1
    assert c3.order == 2
    assert c2.order == 3
    assert c4.order == 4


@pytest.mark.django_db
def test_backlog_move_up_zero():
    user = UserFactory()
    lane = KanbanLaneFactory()
    backlog_column = KanbanColumnFactory(lane=lane, order=0, title="Backlog")
    c1 = KanbanCardFactory(column=backlog_column, order=1)
    with pytest.raises(CrmError) as e:
        c1.backlog_move_up(user)
    assert "cannot be moved any higher in the backlog" in str(e.value)


@pytest.mark.django_db
def test_can_complete_on_board():
    lane = KanbanLaneFactory(order=1)
    column = KanbanColumnFactory(lane=lane, order=1, title="Specify")
    card = KanbanCardFactory(column=column, order=1)
    KanbanCardFactory(column=column, order=2)
    assert card.can_complete() is False


@pytest.mark.django_db
@pytest.mark.parametrize(
    "state,expect",
    [
        (KanbanCard.ACTIVE, False),
        (KanbanCard.DONE, True),
        (KanbanCard.TRACKED, False),
    ],
)
def test_can_complete_on_board_final_column(state, expect):
    lane = KanbanLaneFactory(order=1)
    column = KanbanColumnFactory(lane=lane, order=1, title="Specify")
    KanbanCardFactory(column=column, order=1)
    card = KanbanCardFactory(column=column, order=2, state=state)
    assert card.can_complete() is expect


@pytest.mark.django_db
def test_can_complete_on_backlog():
    lane = KanbanLaneFactory(order=1)
    backlog_column = KanbanColumnFactory(lane=lane, order=0, title="Backlog")
    card = KanbanCardFactory(column=backlog_column, order=1)
    assert card.can_complete() is False


@pytest.mark.django_db
def test_can_complete_on_backlog_pending():
    lane = KanbanLaneFactory(order=1)
    backlog_column = KanbanColumnFactory(lane=lane, order=0, title="Backlog")
    card = KanbanCardFactory(column=backlog_column, order=0)
    assert card.can_complete() is True


@pytest.mark.django_db
def test_factory():
    KanbanCardFactory()


@pytest.mark.django_db
def test_create_kanban_card():
    ticket = TicketFactory()
    column = KanbanColumnFactory()
    user = UserFactory()
    card = KanbanCard.objects.create_kanban_card(ticket, column, user)
    assert KanbanCard.ACTIVE == card.state
    assert ticket == card.ticket
    assert column == card.column


@pytest.mark.django_db
def test_create_kanban_card_state():
    card = KanbanCard.objects.create_kanban_card(
        TicketFactory(), KanbanColumnFactory(), UserFactory(), KanbanCard.DONE
    )
    assert KanbanCard.DONE == card.state


@pytest.mark.django_db
def test_init_kanban_card():
    ticket = TicketFactory()
    lane = KanbanLaneFactory()
    column = KanbanColumnFactory(lane=lane, title="Backlog", order=0)
    user = UserFactory()
    card = KanbanCard.objects.init_kanban_card(ticket, lane, user)
    assert KanbanCard.ACTIVE == card.state
    assert ticket == card.ticket
    assert column == card.column


@pytest.mark.django_db
def test_init_kanban_card_new_lane():
    """Change the lane for a card."""
    user = UserFactory()
    ticket = TicketFactory(title="My Ticket")
    # lane 1
    lane_1 = KanbanLaneFactory(title="lane_1")
    column_1 = KanbanColumnFactory(lane=lane_1, order=0, title="Backlog")
    KanbanColumnFactory(lane=lane_1, order=1, title="Specify")
    # card
    KanbanCardFactory(ticket=ticket, column=column_1, state=KanbanCard.DONE)
    # lane 2
    lane_2 = KanbanLaneFactory(title="lane_2")
    column_2 = KanbanColumnFactory(lane=lane_2, order=0, title="Backlog")
    KanbanColumnFactory(lane=lane_2, order=1, title="Specify")
    # test
    # import ipdb; ipdb.set_trace()
    KanbanCard.objects.init_kanban_card(ticket, lane_2, user)
    card = KanbanCard.objects.get(ticket=ticket)
    assert lane_2 == card.column.lane
    assert column_2 == card.column
    assert 0 == card.order
    assert KanbanCard.ACTIVE == card.state


@pytest.mark.django_db
def test_init_kanban_card_new_lane_on_board():
    """Cannot change the lane for a card."""
    user = UserFactory()
    ticket = TicketFactory(title="My Ticket")
    # lane 1
    lane_1 = KanbanLaneFactory(title="lane_1")
    KanbanColumnFactory(lane=lane_1, order=0, title="Backlog")
    column_1 = KanbanColumnFactory(lane=lane_1, order=1, title="Specify")
    # card
    KanbanCardFactory(ticket=ticket, column=column_1, state=KanbanCard.DONE)
    # lane 2
    lane_2 = KanbanLaneFactory(title="lane_2")
    KanbanColumnFactory(lane=lane_2, order=0, title="Backlog")
    KanbanColumnFactory(lane=lane_2, order=1, title="Specify")
    # test
    # import ipdb; ipdb.set_trace()
    KanbanCard.objects.init_kanban_card(ticket, lane_2, user)
    card = KanbanCard.objects.get(ticket=ticket)
    assert lane_1 == card.column.lane
    assert column_1 == card.column
    assert 0 == card.order
    assert KanbanCard.DONE == card.state


@pytest.mark.parametrize("state", [KanbanCard.DONE, KanbanCard.TRACKED])
@pytest.mark.django_db
def test_init_kanban_card_same_lane(state):
    """Do not change the state of a card when the lane doesn't change."""
    user = UserFactory()
    ticket = TicketFactory()
    lane = KanbanLaneFactory()
    KanbanColumnFactory(lane=lane, order=0, title="Backlog")
    column = KanbanColumnFactory(lane=lane, order=1, title="Specify")
    card = KanbanCardFactory(column=column, ticket=ticket, state=state)
    card = KanbanCard.objects.init_kanban_card(ticket, lane, user)
    card.refresh_from_db()
    assert state == card.state


@pytest.mark.django_db
def test_kanban_active():
    user = UserFactory()
    lane = KanbanLaneFactory()
    backlog_column = KanbanColumnFactory(lane=lane, order=1, title="Specify")
    card = KanbanCardFactory(column=backlog_column, state=KanbanCard.TRACKED)
    card.kanban_active(user)
    card.refresh_from_db()
    assert card.state == KanbanCard.ACTIVE
    audit = KanbanAudit.objects.get(card=card)
    assert audit.column == backlog_column
    assert audit.created.date() == timezone.now().date()
    assert audit.order == 0
    assert audit.state == KanbanCard.ACTIVE
    assert audit.user == user


@pytest.mark.django_db
def test_kanban_active_not_on_kanban_board():
    user = UserFactory()
    lane = KanbanLaneFactory()
    backlog_column = KanbanColumnFactory(lane=lane, order=0, title="Backlog")
    card = KanbanCardFactory(column=backlog_column)
    with pytest.raises(CrmError) as e:
        card.kanban_active(user)
    assert "not on the Kanban board so can't be made active" in str(e.value)


@pytest.mark.django_db
def test_kanban_first_column():
    lane = KanbanLaneFactory()
    column = KanbanColumnFactory(lane=lane, order=1, title="Specify")
    card = KanbanCardFactory(column=column, state=KanbanCard.ACTIVE)
    assert card.kanban_first_column() is True


@pytest.mark.django_db
def test_kanban_first_column_no_columns():
    lane = KanbanLaneFactory()
    backlog_column = KanbanColumnFactory(lane=lane, order=0, title="Specify")
    card = KanbanCardFactory(column=backlog_column, state=KanbanCard.ACTIVE)
    assert card.kanban_first_column() is False


@pytest.mark.django_db
def test_kanban_done():
    user = UserFactory()
    lane = KanbanLaneFactory()
    backlog_column = KanbanColumnFactory(lane=lane, order=1, title="Specify")
    card = KanbanCardFactory(column=backlog_column, state=KanbanCard.ACTIVE)
    card.kanban_done(user)
    card.refresh_from_db()
    assert card.state == KanbanCard.DONE
    audit = KanbanAudit.objects.get(card=card)
    assert audit.column == backlog_column
    assert audit.created.date() == timezone.now().date()
    assert audit.order == 0
    assert audit.state == KanbanCard.DONE
    assert audit.user == user


@pytest.mark.django_db
def test_kanban_done_not_on_kanban_board():
    user = UserFactory()
    lane = KanbanLaneFactory()
    backlog_column = KanbanColumnFactory(lane=lane, order=0, title="Backlog")
    card = KanbanCardFactory(column=backlog_column)
    with pytest.raises(CrmError) as e:
        card.kanban_done(user)
    assert "not on the Kanban board so can't be marked as done" in str(e.value)


@pytest.mark.django_db
def test_kanban_next():
    user = UserFactory()
    lane = KanbanLaneFactory()
    KanbanColumnFactory(lane=lane, order=0, title="Backlog")
    column_1 = KanbanColumnFactory(lane=lane, order=1, title="Specify")
    column_2 = KanbanColumnFactory(lane=lane, order=2, title="Implement")
    KanbanColumnFactory(lane=lane, order=3, title="Verify")
    card = KanbanCardFactory(column=column_1, state=KanbanCard.DONE)
    card.kanban_next(user)
    card.refresh_from_db()
    assert card.state == KanbanCard.ACTIVE
    audit = KanbanAudit.objects.get(card=card)
    assert audit.column == column_2
    assert audit.created.date() == timezone.now().date()
    assert audit.order == 0
    assert audit.state == KanbanCard.ACTIVE
    assert audit.user == user


@pytest.mark.django_db
def test_kanban_previous():
    user = UserFactory()
    lane = KanbanLaneFactory()
    KanbanColumnFactory(lane=lane, order=0, title="Backlog")
    column_1 = KanbanColumnFactory(lane=lane, order=1, title="Specify")
    column_2 = KanbanColumnFactory(lane=lane, order=2, title="Implement")
    KanbanColumnFactory(lane=lane, order=3, title="Verify")
    card = KanbanCardFactory(column=column_2, state=KanbanCard.ACTIVE)
    card.kanban_previous(user)
    card.refresh_from_db()
    assert card.state == KanbanCard.ACTIVE
    assert card.column == column_1
    audit = KanbanAudit.objects.get(card=card)
    assert audit.column == column_1
    assert audit.created.date() == timezone.now().date()
    assert audit.order == 1
    assert audit.state == KanbanCard.ACTIVE
    assert audit.user == user


@pytest.mark.parametrize("state", [KanbanCard.DONE, KanbanCard.TRACKED])
@pytest.mark.django_db
def test_kanban_previous_done(state):
    user = UserFactory()
    lane = KanbanLaneFactory()
    KanbanColumnFactory(lane=lane, order=0, title="Backlog")
    column_1 = KanbanColumnFactory(lane=lane, order=1, title="Specify")
    KanbanColumnFactory(lane=lane, order=2, title="Implement")
    KanbanColumnFactory(lane=lane, order=3, title="Verify")
    card = KanbanCardFactory(column=column_1, state=state)
    with pytest.raises(CrmError) as e:
        card.kanban_previous(user)
    assert (
        "is not 'active' so cannot be moved " "to the previous column"
    ) in str(e.value)


@pytest.mark.django_db
def test_kanban_previous_on_backlog():
    user = UserFactory()
    lane = KanbanLaneFactory()
    backlog_column = KanbanColumnFactory(lane=lane, order=0, title="Backlog")
    KanbanColumnFactory(lane=lane, order=1, title="Specify")
    KanbanColumnFactory(lane=lane, order=2, title="Implement")
    KanbanColumnFactory(lane=lane, order=3, title="Verify")
    card = KanbanCardFactory(column=backlog_column, state=KanbanCard.ACTIVE)
    with pytest.raises(CrmError) as e:
        card.kanban_previous(user)
    assert (
        "is on the backlog, so cannot be " "moved to the previous column"
    ) in str(e.value)


@pytest.mark.django_db
def test_kanban_previous_to_backlog():
    user = UserFactory()
    lane = KanbanLaneFactory()
    backlog_column = KanbanColumnFactory(lane=lane, order=0, title="Backlog")
    column_1 = KanbanColumnFactory(lane=lane, order=1, title="Specify")
    KanbanColumnFactory(lane=lane, order=2, title="Implement")
    KanbanColumnFactory(lane=lane, order=3, title="Verify")
    card = KanbanCardFactory(column=column_1, state=KanbanCard.ACTIVE)
    card.kanban_previous(user)
    card.refresh_from_db()
    assert card.state == KanbanCard.ACTIVE
    assert card.column == backlog_column
    audit = KanbanAudit.objects.get(card=card)
    assert audit.column == backlog_column
    assert audit.created.date() == timezone.now().date()
    assert audit.order == 1
    assert audit.state == KanbanCard.ACTIVE
    assert audit.user == user


@pytest.mark.django_db
def test_kanban_tracked():
    user = UserFactory()
    lane = KanbanLaneFactory()
    backlog_column = KanbanColumnFactory(lane=lane, order=1, title="Specify")
    card = KanbanCardFactory(column=backlog_column, state=KanbanCard.ACTIVE)
    card.kanban_tracked(user)
    card.refresh_from_db()
    assert card.state == KanbanCard.TRACKED
    audit = KanbanAudit.objects.get(card=card)
    assert audit.column == backlog_column
    assert audit.created.date() == timezone.now().date()
    assert audit.order == 0
    assert audit.state == KanbanCard.TRACKED
    assert audit.user == user


@pytest.mark.django_db
def test_kanban_tracked_not_on_kanban_board():
    user = UserFactory()
    lane = KanbanLaneFactory()
    backlog_column = KanbanColumnFactory(lane=lane, order=0, title="Backlog")
    card = KanbanCardFactory(column=backlog_column)
    with pytest.raises(CrmError) as e:
        card.kanban_tracked(user)
    assert "not on the Kanban board so can't be tracked" in str(e.value)


@pytest.mark.django_db
def test_remove_from_backlog():
    user = UserFactory()
    lane = KanbanLaneFactory()
    backlog_column = KanbanColumnFactory(lane=lane, order=0, title="Backlog")
    KanbanCardFactory(column=backlog_column, order=99)
    card = KanbanCardFactory(column=backlog_column, order=9)
    card.remove_from_backlog(user)
    card.refresh_from_db()
    assert 0 == card.order
    audit = KanbanAudit.objects.get(card=card)
    assert audit.column == backlog_column
    assert audit.created.date() == timezone.now().date()
    assert audit.order == 0
    assert audit.state == KanbanCard.ACTIVE
    assert audit.user == user


@pytest.mark.django_db
def test_remove_from_backlog_not_backlog():
    user = UserFactory()
    lane = KanbanLaneFactory()
    backlog_column = KanbanColumnFactory(lane=lane, order=1, title="Specify")
    card = KanbanCardFactory(column=backlog_column, order=0)
    with pytest.raises(CrmError) as e:
        card.remove_from_backlog(user)
    assert "It is not on the backlog" in str(e.value)


@pytest.mark.django_db
def test_remove_from_backlog_not_on_backlog():
    user = UserFactory()
    lane = KanbanLaneFactory()
    backlog_column = KanbanColumnFactory(lane=lane, order=0, title="Backlog")
    card = KanbanCardFactory(column=backlog_column, order=0)
    with pytest.raises(CrmError) as e:
        card.remove_from_backlog(user)
    assert "It is not on the backlog" in str(e.value)


@pytest.mark.django_db
def test_str():
    str(KanbanCardFactory())
