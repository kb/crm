# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse

from crm.tests.factories import IndustryFactory
from login.tests.fixture import perm_check


@pytest.mark.django_db
def test_industry_create(perm_check):
    url = reverse("crm.industry.create")
    perm_check.superuser(url)


@pytest.mark.django_db
def test_industry_list(perm_check):
    IndustryFactory(name="Farming")
    IndustryFactory(name="IT")
    url = reverse("crm.industry.list")
    perm_check.superuser(url)


@pytest.mark.django_db
def test_industry_update(perm_check):
    industry = IndustryFactory(name="Farming")
    url = reverse("crm.industry.update", args=[industry.pk])
    perm_check.superuser(url)
