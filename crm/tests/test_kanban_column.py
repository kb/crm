# -*- encoding: utf-8 -*-
import pytest

from crm.models import KanbanCard, KanbanColumn
from crm.tests.factories import (
    KanbanCardFactory,
    KanbanColumnFactory,
    KanbanLaneFactory,
    TicketFactory,
)
from login.tests.factories import UserFactory


def _column_tickets():
    user = UserFactory()
    lane = KanbanLaneFactory(title="Development", order=1)
    column = KanbanColumnFactory(lane=lane, title="Implement", order=1)
    t1 = TicketFactory(title="t1")
    KanbanCardFactory(ticket=t1, column=column, state=KanbanCard.ACTIVE)
    t2 = TicketFactory(title="t2")
    KanbanCardFactory(ticket=t2, column=column, state=KanbanCard.ACTIVE)
    t3 = TicketFactory(title="t3")
    KanbanCardFactory(ticket=t3, column=column, state=KanbanCard.DONE)
    t4 = TicketFactory(title="t4")
    KanbanCardFactory(ticket=t4, column=column, state=KanbanCard.TRACKED)
    t5 = TicketFactory(title="t5")
    KanbanCardFactory(ticket=t5, column=column, state=KanbanCard.ACTIVE)
    t2.set_complete(user)
    t2.save()
    return column


@pytest.mark.django_db
def test_create_kanban_column():
    lane = KanbanLaneFactory(title="Development", order=1)
    column = KanbanColumn.objects.create_kanban_column(
        lane, "Implement", 1, "Test coverage is above 50%"
    )
    assert 1 == column.order
    assert "Implement" == column.title
    assert "Test coverage is above 50%" == column.help_text


@pytest.mark.django_db
def test_init_kanban_column():
    lane = KanbanLaneFactory(title="Development", order=1)
    column = KanbanColumn.objects.init_kanban_column(
        lane, "Specify", 1, "Each task takes less than 2 days"
    )
    assert 1 == column.order
    assert lane == column.lane
    assert "Specify" == column.title
    assert "Each task takes less than 2 days" == column.help_text


@pytest.mark.django_db
def test_init_kanban_column_change_order():
    lane = KanbanLaneFactory(title="Development", order=0)
    column = KanbanColumn.objects.init_kanban_column(
        lane, "Implement", 1, "Test coverage is above 50%"
    )
    assert 1 == column.order
    column = KanbanColumn.objects.init_kanban_column(
        lane, "Implement", 2, "Test coverage is higher than 70%"
    )
    column.refresh_from_db()
    assert 2 == column.order
    assert lane == column.lane
    assert "Implement" == column.title
    assert "Test coverage is higher than 70%" == column.help_text


@pytest.mark.django_db
def test_is_final():
    lane = KanbanLaneFactory(title="Development", order=0)
    KanbanColumnFactory(lane=lane, title="Backlog", order=0)
    KanbanColumnFactory(lane=lane, title="Specify", order=1)
    KanbanColumnFactory(lane=lane, title="Implement", order=2)
    column = KanbanColumnFactory(lane=lane, title="Verify", order=3)
    assert column.is_final() is True


@pytest.mark.django_db
def test_is_final_not():
    lane = KanbanLaneFactory(title="Development", order=0)
    KanbanColumnFactory(lane=lane, title="Backlog", order=0)
    KanbanColumnFactory(lane=lane, title="Specify", order=1)
    column = KanbanColumnFactory(lane=lane, title="Implement", order=2)
    KanbanColumnFactory(lane=lane, title="Verify", order=3)
    assert column.is_final() is False


@pytest.mark.django_db
def test_is_first():
    lane = KanbanLaneFactory(title="Development", order=0)
    KanbanColumnFactory(lane=lane, title="Backlog", order=0)
    column = KanbanColumnFactory(lane=lane, title="Specify", order=1)
    KanbanColumnFactory(lane=lane, title="Implement", order=2)
    KanbanColumnFactory(lane=lane, title="Verify", order=3)
    assert column.is_first() is True


@pytest.mark.django_db
def test_is_first_not():
    lane = KanbanLaneFactory(title="Development", order=0)
    KanbanColumnFactory(lane=lane, title="Backlog", order=0)
    KanbanColumnFactory(lane=lane, title="Specify", order=1)
    column = KanbanColumnFactory(lane=lane, title="Implement", order=2)
    KanbanColumnFactory(lane=lane, title="Verify", order=3)
    assert column.is_first() is False


@pytest.mark.django_db
def test_is_first_not_backlog():
    lane = KanbanLaneFactory(title="Development", order=0)
    column = KanbanColumnFactory(lane=lane, title="Backlog", order=0)
    KanbanColumnFactory(lane=lane, title="Specify", order=1)
    KanbanColumnFactory(lane=lane, title="Implement", order=2)
    KanbanColumnFactory(lane=lane, title="Verify", order=3)
    assert column.is_first() is False


@pytest.mark.django_db
def test_str():
    str(KanbanColumnFactory())


@pytest.mark.django_db
def test_tickets_active():
    column = _column_tickets()
    assert ["t1", "t5"] == [
        x.title for x in column.tickets_active().order_by("title")
    ]


@pytest.mark.django_db
def test_tickets_done():
    column = _column_tickets()
    assert ["t3"] == [x.title for x in column.tickets_done().order_by("title")]


@pytest.mark.django_db
def test_tickets_tracked():
    column = _column_tickets()
    assert ["t4"] == [
        x.title for x in column.tickets_tracked().order_by("title")
    ]
