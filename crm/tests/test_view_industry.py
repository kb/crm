# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse
from http import HTTPStatus

from crm.models import Industry
from crm.tests.factories import IndustryFactory
from login.tests.factories import TEST_PASSWORD, UserFactory


@pytest.mark.django_db
def test_industry_create(client):
    user = UserFactory(is_superuser=True)
    assert 0 == Industry.objects.count()
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.post(
        reverse("crm.industry.create"),
        {"name": "Farming"},
    )
    assert 1 == Industry.objects.count()
    # check
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert response.url == reverse("crm.industry.list")
    Industry.objects.get(name="Farming")


@pytest.mark.django_db
def test_industry_update(client):
    user = UserFactory(is_superuser=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    x = IndustryFactory(name="Farming")
    response = client.post(
        reverse("crm.industry.update", args=[x.pk]),
        {"name": "Agriculture"},
    )
    # check
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert response.url == reverse("crm.industry.list")
    x.refresh_from_db()
    assert "Agriculture" == x.name
