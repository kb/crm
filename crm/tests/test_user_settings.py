# -*- encoding: utf-8 -*-
import pytest

from login.tests.factories import UserFactory
from .factories import UserSettingsFactory


@pytest.mark.django_db
def test_factory():
    UserSettingsFactory()


@pytest.mark.django_db
def test_str():
    user = UserFactory(username="patrick")
    user_settings = UserSettingsFactory(user=user)
    assert "patrick" == str(user_settings)
