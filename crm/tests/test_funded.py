# -*- encoding: utf-8 -*-
import pytest

from crm.models import Funded
from .factories import FundedFactory


@pytest.mark.django_db
def test_init_funded():
    x = Funded.objects.init_funded(
        Funded.SALES_ORDER, "Sales Order", False, True
    )
    assert "sales-order" == x.slug
    assert "Sales Order" == x.name
    assert x.free_of_charge is False
    assert x.sales_order is True


@pytest.mark.django_db
def test_init_funded_already_exists():
    FundedFactory(
        slug=Funded.SALES_ORDER,
        name="Sales Order",
        free_of_charge=False,
        sales_order=True,
    )
    assert 1 == Funded.objects.count()
    x = Funded.objects.init_funded(Funded.SALES_ORDER, "Sales", True, False)
    assert 1 == Funded.objects.count()
    assert "sales-order" == x.slug
    assert "Sales" == x.name
    assert x.free_of_charge is True
    assert x.sales_order is False


@pytest.mark.django_db
def test_ordering():
    FundedFactory(name="C")
    FundedFactory(name="B")
    FundedFactory(name="A")
    assert ["A", "B", "C"] == [x.name for x in Funded.objects.all()]


@pytest.mark.django_db
def test_str():
    assert "Sales Order" == str(FundedFactory(name="Sales Order"))
