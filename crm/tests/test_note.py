# -*- encoding: utf-8 -*-
import pytest

from dateutil.relativedelta import relativedelta
from django.urls import reverse
from django.utils import timezone

from crm.tests.factories import NoteFactory
from search.tests.helper import check_search_methods


@pytest.mark.django_db
def test_factory():
    NoteFactory()


@pytest.mark.django_db
def test_get_update_url():
    x = NoteFactory()
    assert reverse("crm.note.update", args=[x.pk]) == x.get_update_url()


@pytest.mark.django_db
def test_modified_today():
    assert NoteFactory().modified_today is True


@pytest.mark.django_db
def test_modified_today_23_hours():
    created = timezone.now() - relativedelta(hours=23)
    obj = NoteFactory()
    obj.created = created
    obj.save()
    assert obj.modified_today is True


@pytest.mark.django_db
def test_modified_today_25_hours():
    created = timezone.now() - relativedelta(hours=25)
    obj = NoteFactory()
    obj.created = created
    obj.save()
    assert obj.modified_today is False


@pytest.mark.django_db
def test_modified_today_yesterday():
    created = timezone.now() - relativedelta(days=1)
    obj = NoteFactory()
    obj.created = created
    obj.save()
    assert obj.modified_today is False


@pytest.mark.django_db
def test_search_methods():
    check_search_methods(NoteFactory())


@pytest.mark.django_db
def test_str():
    str(NoteFactory())
