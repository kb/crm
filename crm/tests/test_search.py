# -*- encoding: utf-8 -*-
import pytest

from contact.tests.factories import ContactFactory
from crm.search import TicketIndex
from crm.tests.factories import TicketFactory
from login.tests.factories import UserFactory
from search.models import SearchFormat
from search.search import SearchIndex


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_drop_create():
    search_index = SearchIndex(TicketIndex())
    search_index.drop_create()


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_search():
    c1 = ContactFactory(
        company_name="", user=UserFactory(first_name="P", last_name="Kimber")
    )
    c2 = ContactFactory(
        company_name="", user=UserFactory(first_name="A", last_name="Kimber")
    )
    c3 = ContactFactory(
        company_name="", user=UserFactory(first_name="T", last_name="Kimber")
    )
    t1 = TicketFactory(contact=c1, title="Apple", description="Zoom")
    t2 = TicketFactory(contact=c2, title="Apple", description="Apple")
    TicketFactory(contact=c3, title="Banana", description="Orange")
    index = SearchIndex(TicketIndex())
    assert 3 == index.rebuild()
    result, total = index.search("apple", data_format=SearchFormat.COLUMN)
    assert 2 == total
    assert [t2.pk, t1.pk] == [x.pk for x in result]
    check = []
    for row in result:
        col_list = []
        for column in row.data:
            line_list = []
            for line in column:
                line_list.append(line.text)
            col_list.append(line_list)
        check.append(col_list)
    assert [
        [["{0:06d}".format(t2.pk)], ["A Kimber", "Apple", "Apple"]],
        [["{0:06d}".format(t1.pk)], ["P Kimber", "Apple", "Zoom"]],
    ] == check


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_search_ticket_number():
    TicketFactory(title="Apple", description="Apple")
    t2 = TicketFactory(title="Apple", description="Apple")
    index = SearchIndex(TicketIndex())
    assert 2 == index.rebuild()
    result, total = index.search(t2.pk, data_format=SearchFormat.COLUMN)
    assert 1 == total
    assert [t2.pk] == [x.pk for x in result]


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_update():
    TicketFactory()
    TicketFactory()
    TicketFactory()
    search_index = SearchIndex(TicketIndex())
    search_index.drop_create()
    assert 3 == search_index.update()
