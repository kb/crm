# -*- encoding: utf-8 -*-
import factory

from crm.models import (
    CrmContact,
    CrmSettings,
    Funded,
    Industry,
    KanbanCard,
    KanbanColumn,
    KanbanLane,
    Note,
    Priority,
    Ticket,
    UserSettings,
    UserTicketFavourite,
)
from contact.tests.factories import ContactFactory
from login.tests.factories import UserFactory


class FundedFactory(factory.django.DjangoModelFactory):
    free_of_charge = False
    sales_order = False

    class Meta:
        model = Funded

    @factory.sequence
    def name(n):
        return "name_{:02d}".format(n)

    @factory.sequence
    def slug(n):
        return "slug_{:02d}".format(n)


class IndustryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Industry

    @factory.sequence
    def name(n):
        return "name_{:02d}".format(n)


class CrmContactFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = CrmContact

    contact = factory.SubFactory(ContactFactory)
    industry = factory.SubFactory(IndustryFactory)


class KanbanLaneFactory(factory.django.DjangoModelFactory):
    funded = False

    class Meta:
        model = KanbanLane

    @factory.sequence
    def order(n):
        """Order '0' is not shown on a Kanban board."""
        return n + 1

    @factory.sequence
    def title(n):
        return "title_{:02d}".format(n)


class KanbanColumnFactory(factory.django.DjangoModelFactory):
    lane = factory.SubFactory(KanbanLaneFactory)

    class Meta:
        model = KanbanColumn

    @factory.sequence
    def order(n):
        """Order '0' is the backlog (not shown on Kanban board)."""
        return n + 1

    @factory.sequence
    def title(n):
        return "title_{:02d}".format(n)


class PriorityFactory(factory.django.DjangoModelFactory):
    level = 1

    class Meta:
        model = Priority

    @factory.sequence
    def name(n):
        return "name_{:02d}".format(n)


class TicketFactory(factory.django.DjangoModelFactory):
    contact = factory.SubFactory(ContactFactory)
    priority = factory.SubFactory(PriorityFactory)
    user = factory.SubFactory(UserFactory)

    class Meta:
        model = Ticket

    @factory.sequence
    def title(n):
        return "Ticket Title {:02d}".format(n)


class KanbanCardFactory(factory.django.DjangoModelFactory):
    column = factory.SubFactory(KanbanColumnFactory)
    ticket = factory.SubFactory(TicketFactory)

    class Meta:
        model = KanbanCard


class NoteFactory(factory.django.DjangoModelFactory):
    ticket = factory.SubFactory(TicketFactory)
    user = factory.SubFactory(UserFactory)

    class Meta:
        model = Note

    @factory.sequence
    def title(n):
        return "title_{:02d}".format(n)


class UserSettingsFactory(factory.django.DjangoModelFactory):
    user = factory.SubFactory(UserFactory)

    class Meta:
        model = UserSettings


class UserTicketFavouriteFactory(factory.django.DjangoModelFactory):
    ticket = factory.SubFactory(TicketFactory)
    user = factory.SubFactory(UserFactory)

    class Meta:
        model = UserTicketFavourite


class CrmSettingsFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = CrmSettings

    project_lane = factory.SubFactory(KanbanLaneFactory)
