# -*- encoding: utf-8 -*-
import dramatiq
import logging

from django.conf import settings
from django.utils import timezone

from crm.search import TicketIndex
from search.search import SearchIndex


logger = logging.getLogger(__name__)


@dramatiq.actor(queue_name=settings.DRAMATIQ_QUEUE_NAME)
def rebuild_ticket_index():
    logger.info("Drop and rebuild ticket index...")
    index = SearchIndex(TicketIndex())
    count = index.rebuild()
    logger.info("Ticket index rebuilt - {} records - complete".format(count))
    return count


@dramatiq.actor(queue_name=settings.DRAMATIQ_QUEUE_NAME)
def update_ticket_index(pk):
    logger.info("Update ticket index {}".format(pk))
    index = SearchIndex(TicketIndex())
    count = index.update(pk)
    logger.info("Updated index for ticket {} ({} record)".format(pk, count))
    return count


def schedule_rebuild_ticket_index():
    logger.info(
        ">>> schedule_rebuild_ticket_index: {}".format(
            timezone.localtime(timezone.now()).strftime("%d/%m/%Y %H:%M")
        )
    )
    rebuild_ticket_index.send()
