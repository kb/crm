crm
***

Django CRM application

Install
=======

Virtual Environment
-------------------

::

  virtualenv --python=python3 venv-crm
  # or
  python3 -m venv venv-crm
  source venv-crm/bin/activate

  pip install -r requirements/local.txt

Testing
=======

::

  find . -name '*.pyc' -delete
  py.test -x

If you **do not** have Elasticsearch installed::

  pytest -x -m "not elasticsearch"

.. note:: We should add Elasticsearch to our GitLab testing configuration, so
          we don't have to include / exclude Elasticsearch tests:
          https://www.docker.elastic.co/

Usage
=====

::

  ./init_dev.sh

Release
=======

https://www.kbsoftware.co.uk/docs/
