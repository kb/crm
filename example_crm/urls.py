# -*- encoding: utf-8 -*-
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import include, re_path
from django.urls import path, reverse_lazy
from django.views.generic import RedirectView
from rest_framework import routers
from rest_framework.authtoken import views

from contact.api import ContactViewSet
from crm.views import TicketViewSet
from .views import (
    ContactDetailTicketListView,
    ContactListView,
    HomeView,
    InvoiceListView,
    SettingsView,
    TicketDetailView,
)


router = routers.DefaultRouter()
router.register(r"contacts", ContactViewSet, basename="contact")
router.register(r"tickets", TicketViewSet, basename="ticket")


urlpatterns = [
    # DOES NOT USE A TRAILING SLASH
    # http://localhost:8000/api/0.1/contact
    re_path(r"^api/0.1/", view=include((router.urls, "api"), namespace="api")),
    re_path(r"^$", view=HomeView.as_view(), name="project.home"),
    re_path(r"^", view=include("login.urls")),
    re_path(r"^api/", view=include("crm.urls_api")),
    re_path(r"^chat/", view=include("chat.urls")),
    re_path(r"^contact/$", view=ContactListView.as_view(), name="contact.list"),
    re_path(
        r"^contact/(?P<pk>\d+)/$",
        view=ContactDetailTicketListView.as_view(),
        name="contact.detail",
    ),
    re_path(r"^contact/", view=include("contact.urls")),
    re_path(r"^crm/", view=include("crm.urls")),
    re_path(
        r"^crm/ticket/(?P<pk>\d+)/$",
        view=TicketDetailView.as_view(),
        name="ticket.detail",
    ),
    re_path(r"^invoice/$", view=InvoiceListView.as_view(), name="invoice.list"),
    re_path(r"^invoice/", view=include("invoice.urls")),
    re_path(
        r"^settings/$",
        view=SettingsView.as_view(),
        name="project.settings",
    ),
    re_path(r"^token/$", view=views.obtain_auth_token, name="api.token.auth"),
    re_path(r"^wizard/", view=include("block.urls.wizard")),
    re_path(
        r"^home/user/$",
        view=RedirectView.as_view(url=reverse_lazy("crm.ticket.home")),
        name="project.dash",
    ),
]

urlpatterns += staticfiles_urlpatterns()

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        re_path(r"^__debug__/", include(debug_toolbar.urls))
    ] + urlpatterns
