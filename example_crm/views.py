# -*- encoding: utf-8 -*-
from braces.views import LoginRequiredMixin, StaffuserRequiredMixin
from django.views.generic import ListView, TemplateView

from base.view_utils import BaseMixin, RedirectNextMixin
from contact.views import ContactListMixin, ContactPermMixin
from crm.views import ContactTicketListMixin, TicketDetailMixin
from invoice.views import InvoiceListMixin


class ContactDetailTicketListView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    ContactTicketListMixin,
    BaseMixin,
    ListView,
):
    paginate_by = 20
    template_name = "example/contact_detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({"next": "the-next-url"})
        return context

    def test_contact(self):
        return self._contact()


class ContactListView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    ContactListMixin,
    BaseMixin,
    ListView,
):
    template_name = "example/contact_list.html"


class HomeView(TemplateView):
    template_name = "example/home.html"


class InvoiceListView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    InvoiceListMixin,
    BaseMixin,
    ListView,
):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({"show_download": True})
        return context


class SettingsView(TemplateView):
    template_name = "example/settings.html"


class TicketDetailView(
    LoginRequiredMixin,
    RedirectNextMixin,
    TicketDetailMixin,
    ContactPermMixin,
    BaseMixin,
    ListView,
):
    template_name = "example/ticket_detail.html"
