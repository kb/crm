# -*- encoding: utf-8 -*-
import pytest
import pytz

from datetime import date, datetime
from django.urls import reverse
from freezegun import freeze_time
from http import HTTPStatus
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

from contact.tests.factories import ContactFactory
from crm.tests.factories import PriorityFactory, TicketFactory
from login.tests.factories import UserFactory


@pytest.yield_fixture
def api_client():
    """Create an admin user, and login using the token."""
    user = UserFactory(username="staff", is_staff=True)
    token = Token.objects.create(user=user)
    client = APIClient()
    client.credentials(HTTP_AUTHORIZATION="Token {}".format(token.key))
    yield client


@pytest.mark.django_db
def test_api_ticket(api_client):
    user_1 = UserFactory(first_name="Andrea", username="andrea")
    user_2 = UserFactory(first_name="Patrick", username="patrick")
    contact = ContactFactory(company_name="", user=user_1)
    priority_high = PriorityFactory(name="High")
    priority_medium = PriorityFactory(name="Medium")
    with freeze_time(datetime(2020, 3, 11, 18, 30, 0, tzinfo=pytz.utc)):
        t1 = TicketFactory(
            contact=contact,
            description="Green, green grass",
            priority=priority_medium,
            title="Mow the lawn",
            user=user_1,
            user_assigned=user_2,
        )
        t2 = TicketFactory(
            contact=contact,
            description="Nice and strong please",
            priority=priority_high,
            title="Make a cup of tea",
            user=user_2,
            user_assigned=user_1,
        )
    # get
    response = api_client.get(reverse("api.crm.ticket"))
    assert HTTPStatus.OK == response.status_code
    data = response.json()
    assert {
        "data": [
            {
                "type": "ticketAPIViews",
                "id": f"{t1.pk}",
                "attributes": {
                    "complete": None,
                    "contact-name": "Andrea",
                    "created": "2020-03-11T18:30:00Z",
                    "description": "Green, green grass",
                    "due": None,
                    "priority-name": "Medium",
                    "title": "Mow the lawn",
                    "user-assigned-name": "Patrick",
                },
                "relationships": {
                    "contact": {
                        "data": {"type": "contact", "id": f"{contact.pk}"}
                    },
                    "priority": {
                        "data": {
                            "type": "priorities",
                            "id": f"{priority_medium.pk}",
                        }
                    },
                    "user": {"data": {"type": "users", "id": f"{user_1.pk}"}},
                    "user-assigned": {
                        "data": {"id": f"{user_2.pk}", "type": "users"}
                    },
                },
            },
            {
                "type": "ticketAPIViews",
                "id": f"{t2.pk}",
                "attributes": {
                    "complete": None,
                    "contact-name": "Andrea",
                    "created": "2020-03-11T18:30:00Z",
                    "description": "Nice and strong please",
                    "due": None,
                    "priority-name": "High",
                    "title": "Make a cup of tea",
                    "user-assigned-name": "Andrea",
                },
                "relationships": {
                    "contact": {
                        "data": {"type": "contact", "id": f"{contact.pk}"}
                    },
                    "priority": {
                        "data": {
                            "type": "priorities",
                            "id": f"{priority_high.pk}",
                        }
                    },
                    "user": {"data": {"type": "users", "id": f"{user_2.pk}"}},
                    "user-assigned": {
                        "data": {"id": f"{user_1.pk}", "type": "users"}
                    },
                },
            },
        ]
    } == data
