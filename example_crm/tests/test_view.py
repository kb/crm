# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse
from http import HTTPStatus

from contact.tests.factories import ContactFactory, UserContactFactory
from crm.tests.factories import CrmContactFactory
from login.tests.factories import TEST_PASSWORD, UserFactory


@pytest.mark.django_db
def test_contact_detail(client):
    user = UserFactory(is_staff=True)
    UserContactFactory(user=UserFactory())
    contact = ContactFactory()
    CrmContactFactory(contact=contact)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(reverse("contact.detail", args=[contact.pk]))
    assert HTTPStatus.OK == response.status_code
    assert "contact" in response.context
    assert "contact_addresses" in response.context
    assert "contact_emails" in response.context
    assert "contact_phones" in response.context
    assert "crm_contact" in response.context
    assert "invoice_contact" in response.context
    assert "ticket_list" in response.context
