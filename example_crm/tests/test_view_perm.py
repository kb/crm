# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse

from contact.tests.factories import ContactFactory, UserContactFactory
from crm.tests.factories import CrmContactFactory, TicketFactory
from invoice.tests.factories import InvoiceContactFactory
from login.tests.factories import TEST_PASSWORD, UserFactory
from login.tests.fixture import perm_check
from login.tests.scenario import get_user_web


@pytest.mark.django_db
def test_contact_detail(perm_check):
    contact = ContactFactory()
    CrmContactFactory(contact=contact)
    url = reverse("contact.detail", args=[contact.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_contact_list(perm_check):
    perm_check.staff(reverse("contact.list"))


@pytest.mark.django_db
def test_contact_detail_user_contact(perm_check):
    """TODO PJK Not sure what this test is testing!"""
    UserContactFactory(user=get_user_web())
    user_contact = UserContactFactory()
    InvoiceContactFactory(contact=user_contact.contact)
    UserContactFactory(contact=user_contact.contact)
    url = reverse("contact.detail", args=[user_contact.contact.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_invoice_list(perm_check):
    url = reverse("invoice.list")
    perm_check.staff(url)
